<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$appDIR = "/var/www/klimeto/projects/2017/mzp/apps/testovanie_inspire_zdrojov/"; 
$data = file_get_contents("php://input");
$endpoint = explode('?',$data)[0];
$url="https://bolegweb.geof.unizg.hr/pycsw/inspire-testing";
$now = date('Y_m_d_H_i_s');
$cswGetRecordsURL = $url . '?service=CSW&version=2.0.2&request=GetRecords&outputSchema=http://www.isotc211.org/2005/gmd&typenames=gmd:MD_Metadata&elementsetname=full&resulttype=results&constraintlanguage=FILTER&constraint=%3Cogc:Filter%20xmlns:ogc%3D%22http://www.opengis.net/ogc%22%3E%3Cogc:PropertyIsEqualTo%3E%3Cogc:PropertyName%3Edc:source%3C/ogc:PropertyName%3E%3Cogc:Literal%3E'.$endpoint.'%3C/ogc:Literal%3E%3C/ogc:PropertyIsEqualTo%3E%3C/ogc:Filter%3E';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $cswGetRecordsURL);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
$result = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
$p = xml_parser_create();
xml_parse_into_struct($p, $result, $vals, $index);
xml_parser_free($p);
$recordsMatched = $vals[3]['attributes']['NUMBEROFRECORDSMATCHED'];
if ($recordsMatched){
	$serviceMDUUID = $vals[6]['value'];
	$out['status'] = 1;
	$out['metadata']= $url .'?service=CSW&version=2.0.2&OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&ELEMENTSETNAME=full&Request=GetRecordById&id=' . $serviceMDUUID;
	file_put_contents($appDIR . 'data/xml/wxs2iso/' .$serviceMDUUID. '.xml', fopen($out['metadata'], 'r'));
}
else{

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $data);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	$result = curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//var_dump($httpCode);
	curl_close($ch);
	//var_dump($result);
	if (strpos($result, 'WFS_Capabilities') !== false) {
		$wxstype = 'WFS';
		$requestXML = '<Harvest xmlns="http://www.opengis.net/cat/csw/2.0.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-publication.xsd" service="CSW" version="2.0.2"><Source>'.$endpoint.'</Source><ResourceType>http://www.opengis.net/wfs</ResourceType><ResourceFormat>application/xml</ResourceFormat></Harvest>';
	}else if(strpos($result, 'WMS_Capabilities') !== false){
		$wxstype = 'WMS';
		$requestXML = '<Harvest xmlns="http://www.opengis.net/cat/csw/2.0.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-publication.xsd" service="CSW" version="2.0.2"><Source>'.$endpoint.'</Source><ResourceType>http://www.opengis.net/wms</ResourceType><ResourceFormat>application/xml</ResourceFormat></Harvest>';
		//var_dump($requestXML);
	}else if(strpos($result, 'sos:Capabilities') !== false){
		$wxstype = 'SOS';
		$requestXML = '<Harvest xmlns="http://www.opengis.net/cat/csw/2.0.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-publication.xsd" service="CSW" version="2.0.2"><Source>'.$endpoint.'</Source><ResourceType>http://www.opengis.net/sos/1.0</ResourceType><ResourceFormat>application/xml</ResourceFormat></Harvest>';
	}else if(strpos($result, 'wcs:Capabilities') !== false){
		$wxstype = 'WCS';
		$requestXML = '<Harvest xmlns="http://www.opengis.net/cat/csw/2.0.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-publication.xsd" service="CSW" version="2.0.2"><Source>'.$endpoint.'</Source><ResourceType>http://www.opengis.net/wcs</ResourceType><ResourceFormat>application/xml</ResourceFormat></Harvest>';
	}else if (strpos($result, 'wps:Capabilities') !== false){
		$wxstype = 'WPS';
		$requestXML = '<Harvest xmlns="http://www.opengis.net/cat/csw/2.0.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-publication.xsd" service="CSW" version="2.0.2"><Source>'.$endpoint.'</Source><ResourceType>http://www.opengis.net/wps/1.0.0</ResourceType><ResourceFormat>application/xml</ResourceFormat></Harvest>';
	}

	if ($wxstype && $requestXML){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		if(curl_exec($ch) === false)
		{
			$out['error'] = "Curl error: " . curl_error($ch);
		}
		else
		{
			//echo "<h3>RESULTS:</h3><p>HTTP POST Operation completed without any errors.</p>";
		}		
		curl_close($ch);
		$p = xml_parser_create();
		xml_parse_into_struct($p, $output, $vals, $index);
		xml_parser_free($p);
		//var_dump($vals);
		if ($vals[3]['tag'] == 'CSW:TOTALINSERTED')
		{
			if ($vals[13]['tag'] == 'DC:IDENTIFIER'){
					$serviceMDUUID = $vals[13]['value'];
					$out['metadata'] = $url .'?service=CSW&version=2.0.2&OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&ELEMENTSETNAME=full&Request=GetRecordById&id=' . $serviceMDUUID;
					$out['status'] = 0;
					file_put_contents($appDIR . 'data/xml/wxs2iso/' .$serviceMDUUID. '.xml', fopen($out['metadata'], 'r'));
				}
			}
			
		}
}
echo json_encode($out,JSON_UNESCAPED_SLASHES);
?>
