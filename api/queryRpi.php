<?php

$data = file_get_contents("php://input");
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://rpi.gov.sk/client/map/api/sk/records/' . $data); 
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
$result = curl_exec($ch);
curl_close($ch);
$obj = json_decode($result, true);
echo json_encode($obj,JSON_UNESCAPED_SLASHES);

?>