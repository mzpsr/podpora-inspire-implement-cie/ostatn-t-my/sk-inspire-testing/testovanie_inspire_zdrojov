31.01.2018 01:26:37 - Preparing Test Run interoperability_null_GeodeticandCartographicInstituteBratislava_cp:CadastralZoning (initiated Wed Jan 31 01:26:37 CET 2018)
31.01.2018 01:26:37 - Resolving Executable Test Suite dependencies
31.01.2018 01:26:37 - Preparing 10 Test Task:
31.01.2018 01:26:37 -  TestTask 1 (c14d2293-6369-45ba-b1e8-6565c60949de)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 2 (abb35739-c3b5-4cf8-a7be-9e3fd13672c5)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.09820daf-62b2-4fa3-a95f-56a0d2b7c4d8'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 3 (d51deaa4-4075-4960-83bc-982cc6164a79)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.18b742d0-15eb-421f-bbec-7c8c5cf7ee1a'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 4 (2d45baa5-3091-4d08-be03-0c008e8b37d1)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Application Schema, Cadastral parcel (EID: 1f9bc92a-5879-4e9b-bcbe-1d2d0cab0aab, V: 0.2.2 )'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 5 (482050ae-de2d-4c12-8037-754410e87eea)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 6 (34e917af-eb65-479b-bffd-913f04b2b6a1)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Data consistency, Cadastral Parcels (EID: 92032cdb-db88-42aa-96c0-70a1af9e68b1, V: 0.2.0 )'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 7 (749cb889-4637-4736-8fcb-affa82972113)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 8 (46951350-cedf-4e8f-9d55-bbad8c8de36e)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Cadastral Parcels (EID: c4fbae00-3070-49fa-b803-24c66c31ac70, V: 0.2.1 )'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 9 (1b3c58d3-9c9e-4d06-b1bc-1471db37431f)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 -  TestTask 10 (df34dc7f-0baa-4347-ac32-dc502f79063c)
31.01.2018 01:26:37 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Reference Systems, Cadastral Parcels (EID: dbcc48ae-6871-4444-8e95-547bc22aacb2, V: 0.2.1 )'
31.01.2018 01:26:37 -  with parameters: 
31.01.2018 01:26:37 - etf.testcases = *
31.01.2018 01:26:37 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 01:26:37 - Setting state to CREATED
31.01.2018 01:26:37 - Changed state from CREATED to INITIALIZING
31.01.2018 01:26:37 - Starting TestRun.f93526cc-dd48-4356-8f25-101e37a9a3a6 at 2018-01-31T01:26:38+01:00
31.01.2018 01:26:38 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 01:26:38 - TestRunTask initialized
31.01.2018 01:26:38 - Creating new tests databases to speed up tests.
31.01.2018 01:26:38 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:38 - Optimizing last database etf-tdb-390c0070-598f-4b93-ace8-d5cbc05b2f82-0 
31.01.2018 01:26:38 - Import completed
31.01.2018 01:26:40 - Validation ended with 0 error(s)
31.01.2018 01:26:40 - Compiling test script
31.01.2018 01:26:40 - Starting XQuery tests
31.01.2018 01:26:40 - "Testing 1 features"
31.01.2018 01:26:40 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
31.01.2018 01:26:40 - "Statistics table: 0 ms"
31.01.2018 01:26:40 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
31.01.2018 01:26:40 - "Test Case 'Basic tests' started"
31.01.2018 01:26:40 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
31.01.2018 01:26:40 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
31.01.2018 01:26:40 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
31.01.2018 01:26:40 - "Test Case 'Basic tests' finished: PASSED"
31.01.2018 01:26:40 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
31.01.2018 01:26:40 - Releasing resources
31.01.2018 01:26:40 - TestRunTask initialized
31.01.2018 01:26:40 - Recreating new tests databases as the Test Object has changed!
31.01.2018 01:26:40 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:40 - Optimizing last database etf-tdb-390c0070-598f-4b93-ace8-d5cbc05b2f82-0 
31.01.2018 01:26:40 - Import completed
31.01.2018 01:26:40 - Validation ended with 0 error(s)
31.01.2018 01:26:40 - Compiling test script
31.01.2018 01:26:40 - Starting XQuery tests
31.01.2018 01:26:40 - "Testing 1 features"
31.01.2018 01:26:41 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 01:26:41 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
31.01.2018 01:26:41 - "Statistics table: 0 ms"
31.01.2018 01:26:41 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
31.01.2018 01:26:41 - "Test Case 'Schema' started"
31.01.2018 01:26:41 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
31.01.2018 01:26:41 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
31.01.2018 01:26:41 - "Test Case 'Schema' finished: PASSED_MANUAL"
31.01.2018 01:26:41 - "Test Case 'Schema validation' started"
31.01.2018 01:26:41 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
31.01.2018 01:26:41 - "Validating service.xml"
31.01.2018 01:26:46 - "Duration: 5438 ms. Errors: 0."
31.01.2018 01:26:46 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 5438 ms"
31.01.2018 01:26:46 - "Test Case 'Schema validation' finished: PASSED"
31.01.2018 01:26:46 - "Test Case 'GML model' started"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 1 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Case 'GML model' finished: PASSED"
31.01.2018 01:26:46 - "Test Case 'Simple features' started"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 1 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Case 'Simple features' finished: PASSED"
31.01.2018 01:26:46 - "Test Case 'Code list values in basic data types' started"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 7 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 4 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 3 ms"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
31.01.2018 01:26:46 - "Test Case 'Code list values in basic data types' finished: PASSED"
31.01.2018 01:26:46 - "Test Case 'Constraints' started"
31.01.2018 01:26:46 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
31.01.2018 01:26:46 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 01:26:46 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
31.01.2018 01:26:47 - Releasing resources
31.01.2018 01:26:47 - TestRunTask initialized
31.01.2018 01:26:47 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:47 - Validation ended with 0 error(s)
31.01.2018 01:26:47 - Compiling test script
31.01.2018 01:26:47 - Starting XQuery tests
31.01.2018 01:26:47 - "Testing 1 features"
31.01.2018 01:26:47 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-gml/ets-cp-gml-bsxets.xml"
31.01.2018 01:26:47 - "Statistics table: 0 ms"
31.01.2018 01:26:47 - "Test Suite 'Conformance class: GML application schemas, Cadastral Parcels' started"
31.01.2018 01:26:47 - "Test Case 'Basic test' started"
31.01.2018 01:26:47 - "Test Assertion 'cp-gml.a.1: CadastralParcel feature in dataset': FAILED - 1 ms"
31.01.2018 01:26:47 - "Test Case 'Basic test' finished: FAILED"
31.01.2018 01:26:47 - "Test Suite 'Conformance class: GML application schemas, Cadastral Parcels' finished: FAILED"
31.01.2018 01:26:47 - Releasing resources
31.01.2018 01:26:47 - TestRunTask initialized
31.01.2018 01:26:47 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:47 - Validation ended with 0 error(s)
31.01.2018 01:26:47 - Compiling test script
31.01.2018 01:26:47 - Starting XQuery tests
31.01.2018 01:26:47 - "Testing 1 features"
31.01.2018 01:26:47 - "Indexing features (parsing errors: 0): 42 ms"
31.01.2018 01:26:47 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-as/ets-cp-as-bsxets.xml"
31.01.2018 01:26:47 - "Statistics table: 1 ms"
31.01.2018 01:26:47 - "Test Suite 'Conformance class: Application Schema, Cadastral parcel' started"
31.01.2018 01:26:47 - "Test Case 'Modelling of object references' started"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.a.1: Verify whether all instances of the spatial object type CadastralParcel carry as a thematic identifier the attribute nationalCadastralReference.': PASSED - 0 ms"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.a.2: Manual review - Verify that the value of the attribute nationalCadastralReference enables the users to make the link with rights, owners and other cadastral information in national cadastral registers or equivalent.': PASSED_MAN..."
31.01.2018 01:26:47 - "Test Case 'Modelling of object references' finished: PASSED_MANUAL"
31.01.2018 01:26:47 - "Test Case 'Cadastral Boundaries' started"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.b.1: Cadastral boundaries corresponding to the outline of a cadastral parcel form closed ring(s).': NOT_APPLICABLE"
31.01.2018 01:26:47 - "Test Case 'Cadastral Boundaries' finished: NOT_APPLICABLE"
31.01.2018 01:26:47 - "Test Case 'Code list values' started"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.c.1: CadastralZoningLevel attributes': PASSED - 4 ms"
31.01.2018 01:26:47 - "Test Case 'Code list values' finished: PASSED"
31.01.2018 01:26:47 - "Test Case 'Constraints' started"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.d.1: Value of areaValue shall be given in square meters': PASSED - 0 ms"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.d.2: Value of estimatedAccuracy shall be given in meters': FAILED - 0 ms"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.d.3: Type of geometry shall be GM_Surface or GM_MultiSurface': PASSED - 0 ms"
31.01.2018 01:26:47 - "Test Assertion 'cp-as.d.4: A lower level cadastral zoning shall be part of an upper level zoning.': PASSED - 0 ms"
31.01.2018 01:26:47 - "Test Case 'Constraints' finished: FAILED"
31.01.2018 01:26:47 - "Test Suite 'Conformance class: Application Schema, Cadastral parcel' finished: FAILED"
31.01.2018 01:26:48 - Releasing resources
31.01.2018 01:26:48 - TestRunTask initialized
31.01.2018 01:26:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:48 - Validation ended with 0 error(s)
31.01.2018 01:26:48 - Compiling test script
31.01.2018 01:26:48 - Starting XQuery tests
31.01.2018 01:26:48 - "Testing 1 features"
31.01.2018 01:26:48 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 01:26:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
31.01.2018 01:26:48 - "Statistics table: 1 ms"
31.01.2018 01:26:48 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
31.01.2018 01:26:48 - "Test Case 'Version consistency' started"
31.01.2018 01:26:48 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
31.01.2018 01:26:48 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
31.01.2018 01:26:48 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
31.01.2018 01:26:48 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
31.01.2018 01:26:48 - "Test Case 'Temporal consistency' started"
31.01.2018 01:26:48 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
31.01.2018 01:26:48 - "Test Case 'Temporal consistency' finished: PASSED"
31.01.2018 01:26:48 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
31.01.2018 01:26:48 - Releasing resources
31.01.2018 01:26:48 - TestRunTask initialized
31.01.2018 01:26:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:48 - Validation ended with 0 error(s)
31.01.2018 01:26:48 - Compiling test script
31.01.2018 01:26:48 - Starting XQuery tests
31.01.2018 01:26:48 - "Testing 1 features"
31.01.2018 01:26:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-dc/ets-cp-dc-bsxets.xml"
31.01.2018 01:26:48 - "Statistics table: 0 ms"
31.01.2018 01:26:48 - "Test Suite 'Conformance class: Data consistency, Cadastral Parcels' started"
31.01.2018 01:26:48 - "Test Case 'Additional theme-specific consistency rules' started"
31.01.2018 01:26:48 - "Test Assertion 'cp-dc.a.1: Test always passes': PASSED - 0 ms"
31.01.2018 01:26:48 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
31.01.2018 01:26:48 - "Test Suite 'Conformance class: Data consistency, Cadastral Parcels' finished: PASSED"
31.01.2018 01:26:48 - Releasing resources
31.01.2018 01:26:48 - TestRunTask initialized
31.01.2018 01:26:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:48 - Validation ended with 0 error(s)
31.01.2018 01:26:48 - Compiling test script
31.01.2018 01:26:48 - Starting XQuery tests
31.01.2018 01:26:48 - "Testing 1 features"
31.01.2018 01:26:49 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 01:26:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
31.01.2018 01:26:49 - "Statistics table: 0 ms"
31.01.2018 01:26:49 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
31.01.2018 01:26:49 - "Test Case 'Coordinate reference systems (CRS)' started"
31.01.2018 01:26:49 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 0 ms"
31.01.2018 01:26:49 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
31.01.2018 01:26:49 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
31.01.2018 01:26:49 - Releasing resources
31.01.2018 01:26:49 - TestRunTask initialized
31.01.2018 01:26:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:49 - Validation ended with 0 error(s)
31.01.2018 01:26:49 - Compiling test script
31.01.2018 01:26:49 - Starting XQuery tests
31.01.2018 01:26:49 - "Testing 1 features"
31.01.2018 01:26:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-ia/ets-cp-ia-bsxets.xml"
31.01.2018 01:26:49 - "Statistics table: 0 ms"
31.01.2018 01:26:49 - "Test Suite 'Conformance class: Information accessibility, Cadastral Parcels' started"
31.01.2018 01:26:49 - "Test Case 'Code lists' started"
31.01.2018 01:26:49 - "Test Assertion 'cp-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
31.01.2018 01:26:49 - "Test Case 'Code lists' finished: PASSED"
31.01.2018 01:26:49 - "Test Case 'Feature references' started"
31.01.2018 01:26:49 - "Test Assertion 'cp-ia.b.1: BasicPropertyUnit.AdministrativeUnit and CadastralParcel.AdministrativeUnit': PASSED - 0 ms"
31.01.2018 01:26:49 - "Test Assertion 'cp-ia.b.2: CadastralBoundary.parcel': PASSED - 0 ms"
31.01.2018 01:26:49 - "Test Assertion 'cp-ia.b.3: CadastralParcel.basicPropertyUnit': PASSED - 0 ms"
31.01.2018 01:26:49 - "Test Assertion 'cp-ia.b.4: CadastralParcel.zoning': PASSED - 0 ms"
31.01.2018 01:26:49 - "Test Assertion 'cp-ia.b.5: CadastralZoning.upperLevelUnit': PASSED - 1 ms"
31.01.2018 01:26:49 - "Test Case 'Feature references' finished: PASSED"
31.01.2018 01:26:49 - "Test Suite 'Conformance class: Information accessibility, Cadastral Parcels' finished: PASSED"
31.01.2018 01:26:49 - Releasing resources
31.01.2018 01:26:49 - TestRunTask initialized
31.01.2018 01:26:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:49 - Validation ended with 0 error(s)
31.01.2018 01:26:49 - Compiling test script
31.01.2018 01:26:49 - Starting XQuery tests
31.01.2018 01:26:50 - "Testing 1 features"
31.01.2018 01:26:50 - "Indexing features (parsing errors: 0): 42 ms"
31.01.2018 01:26:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
31.01.2018 01:26:50 - "Statistics table: 1 ms"
31.01.2018 01:26:50 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
31.01.2018 01:26:50 - "Test Case 'Spatial reference systems' started"
31.01.2018 01:26:50 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
31.01.2018 01:26:50 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
31.01.2018 01:26:50 - "Test Case 'Spatial reference systems' finished: FAILED"
31.01.2018 01:26:50 - "Test Case 'Temporal reference systems' started"
31.01.2018 01:26:50 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 1 ms"
31.01.2018 01:26:50 - "Test Case 'Temporal reference systems' finished: PASSED"
31.01.2018 01:26:50 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
31.01.2018 01:26:50 - Releasing resources
31.01.2018 01:26:50 - TestRunTask initialized
31.01.2018 01:26:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:26:50 - Validation ended with 0 error(s)
31.01.2018 01:26:50 - Compiling test script
31.01.2018 01:26:50 - Starting XQuery tests
31.01.2018 01:26:50 - "Testing 1 features"
31.01.2018 01:26:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-rs/ets-cp-rs-bsxets.xml"
31.01.2018 01:26:50 - "Statistics table: 0 ms"
31.01.2018 01:26:50 - "Test Suite 'Conformance class: Reference Systems, Cadastral Parcels' started"
31.01.2018 01:26:50 - "Test Case 'Lambert Conformal Conic projection' started"
31.01.2018 01:26:50 - "Test Assertion 'cp-rs.a.1: Verify that data related to the spatial data theme Cadastral Parcels made available in plane coordinates using the Lambert Conformal Conic projection are also available in at least one other of the coordinate reference systems s..."
31.01.2018 01:26:50 - "Test Assertion 'cp-rs.a.2: Verify that if there is a reference to the Lambert Conformal Conic projection in the bounding box of the feature collection, there is also a reference to (at least) one other of the coordinate reference systems specified in sect..."
31.01.2018 01:26:50 - "Test Case 'Lambert Conformal Conic projection' finished: PASSED"
31.01.2018 01:26:50 - "Test Suite 'Conformance class: Reference Systems, Cadastral Parcels' finished: PASSED"
31.01.2018 01:26:50 - Releasing resources
31.01.2018 01:26:50 - Changed state from INITIALIZED to RUNNING
31.01.2018 01:26:51 - Duration: 13sec
31.01.2018 01:26:51 - TestRun finished
31.01.2018 01:26:51 - Changed state from RUNNING to COMPLETED
