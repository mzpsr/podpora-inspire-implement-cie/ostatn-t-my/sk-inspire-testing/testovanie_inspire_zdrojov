07.12.2017 10:59:52 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_au:AdministrativeBoundary (initiated Thu Dec 07 10:59:52 CET 2017)
07.12.2017 10:59:52 - Resolving Executable Test Suite dependencies
07.12.2017 10:59:52 - Preparing 10 Test Task:
07.12.2017 10:59:52 -  TestTask 1 (6af90bfe-f872-4dbe-b3d1-aaebd7f1bfe2)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 2 (74de9ce7-bce8-4e0c-b742-17496eb47b17)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.47c569bc-677d-4ce3-8411-e2b29189332a'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 3 (364e49e4-eeca-4754-a82d-d1d7e74ee634)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Administrative Units (EID: ddecef4b-89a3-4f9d-9246-a50b588fa5a2, V: 0.2.1 )'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 4 (636dc882-9975-43f2-a09b-2cf7af228f1d)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 5 (efa4b754-4050-4ef3-8a1b-c54becdc5bb8)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 6 (8bb00644-317d-4528-b7f8-1c5739ad8452)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Administrative Units (EID: acc5931c-4ff0-499f-b916-3cda1603456b, V: 0.2.2 )'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 7 (199e7eda-a6f7-4a61-bdb3-542117c0e93b)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 8 (5804884e-6398-40c1-9095-32b66634709f)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Administrative Units (EID: ee28b75e-5c80-4370-838d-ab1b18e30b13, V: 0.2.1 )'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 9 (4d9006ef-0a29-48e9-8d82-861550509883)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 -  TestTask 10 (8b47f7a9-4073-45de-8d47-2a80f1f093a6)
07.12.2017 10:59:52 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Administrative Units (EID: cafb75f8-5deb-4cca-89df-d3189322e97f, V: 0.2.0 )'
07.12.2017 10:59:52 -  with parameters: 
07.12.2017 10:59:52 - etf.testcases = *
07.12.2017 10:59:52 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
07.12.2017 10:59:52 - Setting state to CREATED
07.12.2017 10:59:52 - Changed state from CREATED to INITIALIZING
07.12.2017 10:59:52 - Starting TestRun.de385908-7e9c-4f85-abb2-0c3adb814ee2 at 2017-12-07T10:59:53+01:00
07.12.2017 10:59:53 - Changed state from INITIALIZING to INITIALIZED
07.12.2017 10:59:53 - TestRunTask initialized
07.12.2017 10:59:53 - Creating new tests databases to speed up tests.
07.12.2017 10:59:53 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
07.12.2017 10:59:53 - Optimizing last database etf-tdb-5ff6c749-1daa-4121-b6c8-0dccfb98dc3b-0 
07.12.2017 10:59:53 - Import completed
07.12.2017 10:59:54 - Validation ended with 0 error(s)
07.12.2017 10:59:54 - Duration: 2sec
07.12.2017 10:59:54 - TestRun finished
