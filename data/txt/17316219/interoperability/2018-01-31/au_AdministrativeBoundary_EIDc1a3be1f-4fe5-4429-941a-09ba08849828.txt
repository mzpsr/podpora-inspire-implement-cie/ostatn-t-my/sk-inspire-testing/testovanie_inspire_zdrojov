31.01.2018 22:50:06 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava(GK&Uacute;)_au:AdministrativeBoundary (initiated Wed Jan 31 22:50:06 CET 2018)
31.01.2018 22:50:06 - Resolving Executable Test Suite dependencies
31.01.2018 22:50:06 - Preparing 10 Test Task:
31.01.2018 22:50:06 -  TestTask 1 (6aaeab77-c2e3-41b9-8d1c-98011f75ede4)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 2 (c8a8e128-7196-49f5-acce-81125765413d)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 3 (fb5a1592-b00e-4f9d-a4ba-7fdd32231819)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.47c569bc-677d-4ce3-8411-e2b29189332a'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 4 (07ccfad8-228f-432a-9030-e38ae690537e)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Administrative Units (EID: ddecef4b-89a3-4f9d-9246-a50b588fa5a2, V: 0.2.1 )'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 5 (0c14c19e-e97c-41f8-b5e7-17a8e13f5e7d)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 6 (38c1cb3f-8bba-4b6f-9b3a-9505ca6540ae)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Administrative Units (EID: acc5931c-4ff0-499f-b916-3cda1603456b, V: 0.2.2 )'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 7 (f5984cc7-8867-46e5-a415-f40cbb552584)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 8 (a16296e2-52c8-4a17-8939-c2227b8de0b8)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Administrative Units (EID: ee28b75e-5c80-4370-838d-ab1b18e30b13, V: 0.2.1 )'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 9 (a801f1be-c067-4832-8c24-45b8aa90bd21)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 -  TestTask 10 (46a72160-d8e7-4220-83dd-904f25a16f14)
31.01.2018 22:50:06 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Administrative Units (EID: cafb75f8-5deb-4cca-89df-d3189322e97f, V: 0.2.0 )'
31.01.2018 22:50:06 -  with parameters: 
31.01.2018 22:50:06 - etf.testcases = *
31.01.2018 22:50:06 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 22:50:06 - Setting state to CREATED
31.01.2018 22:50:06 - Changed state from CREATED to INITIALIZING
31.01.2018 22:50:06 - Starting TestRun.c1a3be1f-4fe5-4429-941a-09ba08849828 at 2018-01-31T22:50:07+01:00
31.01.2018 22:50:07 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 22:50:07 - TestRunTask initialized
31.01.2018 22:50:07 - Creating new tests databases to speed up tests.
31.01.2018 22:50:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:11 - Optimizing last database etf-tdb-595a4d21-9e0e-48ba-95e1-ca71797cb4d7-0 
31.01.2018 22:50:11 - Import completed
31.01.2018 22:50:13 - Validation ended with 0 error(s)
31.01.2018 22:50:13 - Compiling test script
31.01.2018 22:50:13 - Starting XQuery tests
31.01.2018 22:50:13 - "Testing 1 features"
31.01.2018 22:50:13 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
31.01.2018 22:50:13 - "Statistics table: 1 ms"
31.01.2018 22:50:13 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
31.01.2018 22:50:13 - "Test Case 'Basic tests' started"
31.01.2018 22:50:13 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
31.01.2018 22:50:13 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
31.01.2018 22:50:13 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
31.01.2018 22:50:13 - "Test Case 'Basic tests' finished: PASSED"
31.01.2018 22:50:13 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
31.01.2018 22:50:17 - Releasing resources
31.01.2018 22:50:17 - TestRunTask initialized
31.01.2018 22:50:17 - Recreating new tests databases as the Test Object has changed!
31.01.2018 22:50:17 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:17 - Optimizing last database etf-tdb-595a4d21-9e0e-48ba-95e1-ca71797cb4d7-0 
31.01.2018 22:50:17 - Import completed
31.01.2018 22:50:17 - Validation ended with 0 error(s)
31.01.2018 22:50:17 - Compiling test script
31.01.2018 22:50:17 - Starting XQuery tests
31.01.2018 22:50:17 - "Testing 1 features"
31.01.2018 22:50:17 - "Indexing features (parsing errors: 0): 36 ms"
31.01.2018 22:50:17 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
31.01.2018 22:50:17 - "Statistics table: 0 ms"
31.01.2018 22:50:17 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
31.01.2018 22:50:17 - "Test Case 'Schema' started"
31.01.2018 22:50:17 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
31.01.2018 22:50:17 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
31.01.2018 22:50:17 - "Test Case 'Schema' finished: PASSED_MANUAL"
31.01.2018 22:50:17 - "Test Case 'Schema validation' started"
31.01.2018 22:50:17 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
31.01.2018 22:50:17 - "Validating get.xml"
31.01.2018 22:50:26 - "Duration: 8668 ms. Errors: 20."
31.01.2018 22:50:26 - "Test Assertion 'gmlas.b.2: validate XML documents': FAILED - 8668 ms"
31.01.2018 22:50:26 - "Test Case 'Schema validation' finished: FAILED"
31.01.2018 22:50:26 - "Test Case 'GML model' started"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Case 'GML model' finished: PASSED"
31.01.2018 22:50:26 - "Test Case 'Simple features' started"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 1 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 25 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Case 'Simple features' finished: PASSED"
31.01.2018 22:50:26 - "Test Case 'Code list values in basic data types' started"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 10 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 11 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
31.01.2018 22:50:26 - "Test Case 'Code list values in basic data types' finished: PASSED"
31.01.2018 22:50:26 - "Test Case 'Constraints' started"
31.01.2018 22:50:26 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
31.01.2018 22:50:26 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 22:50:26 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: FAILED"
31.01.2018 22:50:28 - Releasing resources
31.01.2018 22:50:28 - TestRunTask initialized
31.01.2018 22:50:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:28 - Validation ended with 0 error(s)
31.01.2018 22:50:28 - Compiling test script
31.01.2018 22:50:28 - Starting XQuery tests
31.01.2018 22:50:28 - "Testing 1 features"
31.01.2018 22:50:28 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-gml/ets-au-gml-bsxets.xml"
31.01.2018 22:50:28 - "Statistics table: 1 ms"
31.01.2018 22:50:28 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' started"
31.01.2018 22:50:28 - "Test Case 'Basic test' started"
31.01.2018 22:50:28 - "Test Assertion 'au-gml.a.1: Administrative Unit feature in dataset': PASSED - 0 ms"
31.01.2018 22:50:28 - "Test Case 'Basic test' finished: PASSED"
31.01.2018 22:50:28 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' finished: PASSED"
31.01.2018 22:50:28 - Releasing resources
31.01.2018 22:50:28 - TestRunTask initialized
31.01.2018 22:50:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:28 - Validation ended with 0 error(s)
31.01.2018 22:50:28 - Compiling test script
31.01.2018 22:50:28 - Starting XQuery tests
31.01.2018 22:50:28 - "Testing 1 features"
31.01.2018 22:50:28 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-as/ets-au-as-bsxets.xml"
31.01.2018 22:50:28 - "Statistics table: 0 ms"
31.01.2018 22:50:28 - "Test Suite 'Conformance class: Application schema, Administrative Units' started"
31.01.2018 22:50:28 - "Test Case 'Code list values' started"
31.01.2018 22:50:28 - "Test Assertion 'au-as.a.1: nationalLevel attributes': PASSED - 5 ms"
31.01.2018 22:50:28 - "Test Case 'Code list values' finished: PASSED"
31.01.2018 22:50:28 - "Test Case 'Constraints' started"
31.01.2018 22:50:28 - "Test Assertion 'au-as.b.1: No unit at highest level can associate units at a higher level': PASSED - 0 ms"
31.01.2018 22:50:28 - "Test Assertion 'au-as.b.2: No unit at lowest level can associate units at lower level': PASSED - 0 ms"
31.01.2018 22:50:28 - "Test Assertion 'au-as.b.3: Association role condominium applies only for administrative units which nationalLevel=1st order': PASSED - 0 ms"
31.01.2018 22:50:28 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 22:50:28 - "Test Suite 'Conformance class: Application schema, Administrative Units' finished: PASSED"
31.01.2018 22:50:29 - Releasing resources
31.01.2018 22:50:29 - TestRunTask initialized
31.01.2018 22:50:29 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:29 - Validation ended with 0 error(s)
31.01.2018 22:50:29 - Compiling test script
31.01.2018 22:50:29 - Starting XQuery tests
31.01.2018 22:50:29 - "Testing 1 features"
31.01.2018 22:50:29 - "Indexing features (parsing errors: 0): 37 ms"
31.01.2018 22:50:29 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
31.01.2018 22:50:29 - "Statistics table: 0 ms"
31.01.2018 22:50:29 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
31.01.2018 22:50:29 - "Test Case 'Version consistency' started"
31.01.2018 22:50:29 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
31.01.2018 22:50:29 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
31.01.2018 22:50:29 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
31.01.2018 22:50:29 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
31.01.2018 22:50:29 - "Test Case 'Temporal consistency' started"
31.01.2018 22:50:29 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
31.01.2018 22:50:29 - "Test Case 'Temporal consistency' finished: PASSED"
31.01.2018 22:50:29 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
31.01.2018 22:50:30 - Releasing resources
31.01.2018 22:50:30 - TestRunTask initialized
31.01.2018 22:50:30 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:30 - Validation ended with 0 error(s)
31.01.2018 22:50:30 - Compiling test script
31.01.2018 22:50:30 - Starting XQuery tests
31.01.2018 22:50:30 - "Testing 1 features"
31.01.2018 22:50:30 - "Indexing features (parsing errors: 0): 61 ms"
31.01.2018 22:50:30 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-dc/ets-au-dc-bsxets.xml"
31.01.2018 22:50:30 - "Statistics table: 1 ms"
31.01.2018 22:50:30 - "Test Suite 'Conformance class: Data consistency, Administrative Units' started"
31.01.2018 22:50:30 - "Test Case 'Higher Hierarchy Consistency' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.a.1: Each administrative unit refers to exactly one administrative unit at a higher level except those corresponding to the country level unit representing a Member State or being a co-administered unit.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Case 'Higher Hierarchy Consistency' finished: PASSED"
31.01.2018 22:50:30 - "Test Case 'Lower Hierarchy Consistency' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.b.1: Each administrative unit refers to a lower level administrative unit, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.b.2: Each administrative unit refers to lower level administrative units on the next level, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Case 'Lower Hierarchy Consistency' finished: PASSED"
31.01.2018 22:50:30 - "Test Case 'Co-administered' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.c.1: Each administrative unit co-administered by two or more administrative units uses the association role administeredBy, and the units co-administering this unit use the inverse role coAdminister.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Case 'Co-administered' finished: PASSED"
31.01.2018 22:50:30 - "Test Case 'Condominium administration' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.d.1: Each condominium is only administered by administrative units at country level.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Case 'Condominium administration' finished: PASSED"
31.01.2018 22:50:30 - "Test Case 'Area' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.e.1: Administrative units having the same level of administrative hierarchy do not conceptually share common areas.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Case 'Area' finished: PASSED"
31.01.2018 22:50:30 - "Test Case 'Boundary' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.f.1: The geometry of each instance administrative boundary corresponds to an edge in the topological structure formed by the complete boundary graph, including the boundaries of all levels.': FAILED - 105 ms"
31.01.2018 22:50:30 - "Test Case 'Boundary' finished: FAILED"
31.01.2018 22:50:30 - "Test Case 'Condominium spatial extent' started"
31.01.2018 22:50:30 - "Test Assertion 'au-dc.g.1: The spatial extent of a condominium is not part of the geometry representing the spatial extent of an administrative unit.': PASSED - 0 ms"
31.01.2018 22:50:30 - "Test Case 'Condominium spatial extent' finished: PASSED"
31.01.2018 22:50:30 - "Test Suite 'Conformance class: Data consistency, Administrative Units' finished: FAILED"
31.01.2018 22:50:31 - Releasing resources
31.01.2018 22:50:31 - TestRunTask initialized
31.01.2018 22:50:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:31 - Validation ended with 0 error(s)
31.01.2018 22:50:31 - Compiling test script
31.01.2018 22:50:31 - Starting XQuery tests
31.01.2018 22:50:31 - "Testing 1 features"
31.01.2018 22:50:31 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 22:50:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
31.01.2018 22:50:31 - "Statistics table: 0 ms"
31.01.2018 22:50:31 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
31.01.2018 22:50:31 - "Test Case 'Coordinate reference systems (CRS)' started"
31.01.2018 22:50:31 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
31.01.2018 22:50:31 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
31.01.2018 22:50:31 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
31.01.2018 22:50:32 - Releasing resources
31.01.2018 22:50:32 - TestRunTask initialized
31.01.2018 22:50:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:50:32 - Validation ended with 0 error(s)
31.01.2018 22:50:32 - Compiling test script
31.01.2018 22:50:32 - Starting XQuery tests
31.01.2018 22:50:32 - "Testing 1 features"
31.01.2018 22:50:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-ia/ets-au-ia-bsxets.xml"
31.01.2018 22:50:32 - "Statistics table: 0 ms"
31.01.2018 22:50:32 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' started"
31.01.2018 22:50:32 - "Test Case 'Code lists' started"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Case 'Code lists' finished: PASSED"
31.01.2018 22:50:32 - "Test Case 'Feature references' started"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.1: AdministrativeUnit.NUTS': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.2: AdministrativeUnit.Condominium': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.3: AdministrativeUnit.upperLevelUnit': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.4: AdministrativeUnit.lowerLevelUnit': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.5: AdministrativeUnit.administeredBy': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.6: AdministrativeUnit.coAdminister': PASSED - 0 ms"
31.01.2018 22:50:32 - "Test Assertion 'au-ia.b.7: AdministrativeUnit.boundary': PASSED - 0 ms"
31.01.2018 22:50:32 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=http://inspire.ec.europa.eu/codelist/Admi..."
31.01.2018 22:51:02 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=http://inspire.ec.euro..."
31.01.2018 22:51:02 - "Test Assertion 'au-ia.b.8: AdministrativeBoundary.admUnit and Condominium.admUnit': FAILED - 30177 ms"
31.01.2018 22:51:02 - "Test Assertion 'au-ia.b.9: MaritimeZone.boundary': PASSED - 0 ms"
31.01.2018 22:51:02 - "Test Assertion 'au-ia.b.10: MaritimeZone.seaArea': PASSED - 0 ms"
31.01.2018 22:51:02 - "Test Assertion 'au-ia.b.11: MaritimeZone.baseline': PASSED - 0 ms"
31.01.2018 22:51:02 - "Test Case 'Feature references' finished: FAILED"
31.01.2018 22:51:02 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' finished: FAILED"
31.01.2018 22:51:03 - Releasing resources
31.01.2018 22:51:03 - TestRunTask initialized
31.01.2018 22:51:03 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:51:03 - Validation ended with 0 error(s)
31.01.2018 22:51:03 - Compiling test script
31.01.2018 22:51:03 - Starting XQuery tests
31.01.2018 22:51:03 - "Testing 1 features"
31.01.2018 22:51:03 - "Indexing features (parsing errors: 0): 74 ms"
31.01.2018 22:51:03 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
31.01.2018 22:51:03 - "Statistics table: 1 ms"
31.01.2018 22:51:03 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
31.01.2018 22:51:03 - "Test Case 'Spatial reference systems' started"
31.01.2018 22:51:03 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
31.01.2018 22:51:03 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
31.01.2018 22:51:03 - "Test Case 'Spatial reference systems' finished: FAILED"
31.01.2018 22:51:03 - "Test Case 'Temporal reference systems' started"
31.01.2018 22:51:03 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
31.01.2018 22:51:03 - "Test Case 'Temporal reference systems' finished: PASSED"
31.01.2018 22:51:03 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
31.01.2018 22:51:04 - Releasing resources
31.01.2018 22:51:04 - TestRunTask initialized
31.01.2018 22:51:04 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:51:04 - Validation ended with 0 error(s)
31.01.2018 22:51:04 - Compiling test script
31.01.2018 22:51:04 - Starting XQuery tests
31.01.2018 22:51:04 - "Testing 1 features"
31.01.2018 22:51:04 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-rs/ets-au-rs-bsxets.xml"
31.01.2018 22:51:04 - "Statistics table: 1 ms"
31.01.2018 22:51:04 - "Test Suite 'Conformance class: Reference systems, Administrative Units' started"
31.01.2018 22:51:04 - "Test Case 'Additional theme-specific rules for reference systems' started"
31.01.2018 22:51:04 - "Test Assertion 'au-rs.a.1: Test always passes': PASSED - 0 ms"
31.01.2018 22:51:04 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
31.01.2018 22:51:04 - "Test Suite 'Conformance class: Reference systems, Administrative Units' finished: PASSED"
31.01.2018 22:51:04 - Releasing resources
31.01.2018 22:51:04 - Changed state from INITIALIZED to RUNNING
31.01.2018 22:51:04 - Duration: 58sec
31.01.2018 22:51:04 - TestRun finished
31.01.2018 22:51:04 - Changed state from RUNNING to COMPLETED
