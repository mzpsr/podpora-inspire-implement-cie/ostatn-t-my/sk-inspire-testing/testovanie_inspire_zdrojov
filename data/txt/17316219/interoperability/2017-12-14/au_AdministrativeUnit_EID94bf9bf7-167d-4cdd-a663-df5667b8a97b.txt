14.12.2017 21:13:19 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_au:AdministrativeUnit (initiated Thu Dec 14 21:13:19 CET 2017)
14.12.2017 21:13:19 - Resolving Executable Test Suite dependencies
14.12.2017 21:13:19 - Preparing 10 Test Task:
14.12.2017 21:13:19 -  TestTask 1 (b7f785d4-b1d3-4021-8731-8ce4e7b03f42)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 2 (36677c78-1f04-4ddd-bb60-851e7a18759e)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 3 (ed8b93f9-629e-44b0-8136-edebc0428d4c)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.47c569bc-677d-4ce3-8411-e2b29189332a'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 4 (c2b5e747-8e51-4a62-b788-5c841e2cd59b)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Administrative Units (EID: ddecef4b-89a3-4f9d-9246-a50b588fa5a2, V: 0.2.1 )'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 5 (ffd26df5-d79f-4bf4-92c5-b696d18a0a81)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 6 (dee829e4-bfd0-4142-885c-8a3feab94ec7)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Administrative Units (EID: acc5931c-4ff0-499f-b916-3cda1603456b, V: 0.2.2 )'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 7 (c0ccfc45-0811-4de9-8cda-5de89a4ece3f)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 8 (434d54fb-bbce-4132-8010-db16e699f7e1)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Administrative Units (EID: ee28b75e-5c80-4370-838d-ab1b18e30b13, V: 0.2.1 )'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 9 (2ace2fde-03a7-4072-bf5d-56ac85df0b1e)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 -  TestTask 10 (1caf2d30-8ff8-41bc-8f1b-b34c6e748cfd)
14.12.2017 21:13:19 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Administrative Units (EID: cafb75f8-5deb-4cca-89df-d3189322e97f, V: 0.2.0 )'
14.12.2017 21:13:19 -  with parameters: 
14.12.2017 21:13:19 - etf.testcases = *
14.12.2017 21:13:19 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
14.12.2017 21:13:19 - Setting state to CREATED
14.12.2017 21:13:19 - Changed state from CREATED to INITIALIZING
14.12.2017 21:13:20 - Starting TestRun.94bf9bf7-167d-4cdd-a663-df5667b8a97b at 2017-12-14T21:13:21+01:00
14.12.2017 21:13:21 - Changed state from INITIALIZING to INITIALIZED
14.12.2017 21:13:21 - TestRunTask initialized
14.12.2017 21:13:21 - Creating new tests databases to speed up tests.
14.12.2017 21:13:21 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:21 - Optimizing last database etf-tdb-9d3051e2-3393-4b24-8588-74d4f3a6e8d7-0 
14.12.2017 21:13:21 - Import completed
14.12.2017 21:13:22 - Validation ended with 0 error(s)
14.12.2017 21:13:22 - Compiling test script
14.12.2017 21:13:22 - Starting XQuery tests
14.12.2017 21:13:22 - "Testing 1 features"
14.12.2017 21:13:22 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
14.12.2017 21:13:22 - "Statistics table: 1 ms"
14.12.2017 21:13:22 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
14.12.2017 21:13:22 - "Test Case 'Basic tests' started"
14.12.2017 21:13:22 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
14.12.2017 21:13:22 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
14.12.2017 21:13:22 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
14.12.2017 21:13:22 - "Test Case 'Basic tests' finished: PASSED"
14.12.2017 21:13:22 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
14.12.2017 21:13:23 - Releasing resources
14.12.2017 21:13:23 - TestRunTask initialized
14.12.2017 21:13:23 - Recreating new tests databases as the Test Object has changed!
14.12.2017 21:13:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:23 - Optimizing last database etf-tdb-9d3051e2-3393-4b24-8588-74d4f3a6e8d7-0 
14.12.2017 21:13:23 - Import completed
14.12.2017 21:13:23 - Validation ended with 0 error(s)
14.12.2017 21:13:23 - Compiling test script
14.12.2017 21:13:23 - Starting XQuery tests
14.12.2017 21:13:23 - "Testing 1 features"
14.12.2017 21:13:23 - "Indexing features (parsing errors: 0): 37 ms"
14.12.2017 21:13:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
14.12.2017 21:13:23 - "Statistics table: 0 ms"
14.12.2017 21:13:23 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
14.12.2017 21:13:23 - "Test Case 'Schema' started"
14.12.2017 21:13:23 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
14.12.2017 21:13:23 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
14.12.2017 21:13:23 - "Test Case 'Schema' finished: PASSED_MANUAL"
14.12.2017 21:13:23 - "Test Case 'Schema validation' started"
14.12.2017 21:13:23 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
14.12.2017 21:13:23 - "Validating get.xml"
14.12.2017 21:13:29 - "Duration: 6335 ms. Errors: 0."
14.12.2017 21:13:29 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 6335 ms"
14.12.2017 21:13:29 - "Test Case 'Schema validation' finished: PASSED"
14.12.2017 21:13:29 - "Test Case 'GML model' started"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Case 'GML model' finished: PASSED"
14.12.2017 21:13:29 - "Test Case 'Simple features' started"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 1 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 1 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 28 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Case 'Simple features' finished: PASSED"
14.12.2017 21:13:29 - "Test Case 'Code list values in basic data types' started"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 8 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 3 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 3 ms"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 2 ms"
14.12.2017 21:13:29 - "Test Case 'Code list values in basic data types' finished: PASSED"
14.12.2017 21:13:29 - "Test Case 'Constraints' started"
14.12.2017 21:13:29 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
14.12.2017 21:13:29 - "Test Case 'Constraints' finished: PASSED"
14.12.2017 21:13:29 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
14.12.2017 21:13:31 - Releasing resources
14.12.2017 21:13:31 - TestRunTask initialized
14.12.2017 21:13:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:31 - Validation ended with 0 error(s)
14.12.2017 21:13:31 - Compiling test script
14.12.2017 21:13:31 - Starting XQuery tests
14.12.2017 21:13:31 - "Testing 1 features"
14.12.2017 21:13:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-gml/ets-au-gml-bsxets.xml"
14.12.2017 21:13:31 - "Statistics table: 0 ms"
14.12.2017 21:13:31 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' started"
14.12.2017 21:13:31 - "Test Case 'Basic test' started"
14.12.2017 21:13:31 - "Test Assertion 'au-gml.a.1: Administrative Unit feature in dataset': PASSED - 0 ms"
14.12.2017 21:13:31 - "Test Case 'Basic test' finished: PASSED"
14.12.2017 21:13:31 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' finished: PASSED"
14.12.2017 21:13:31 - Releasing resources
14.12.2017 21:13:31 - TestRunTask initialized
14.12.2017 21:13:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:31 - Validation ended with 0 error(s)
14.12.2017 21:13:31 - Compiling test script
14.12.2017 21:13:31 - Starting XQuery tests
14.12.2017 21:13:31 - "Testing 1 features"
14.12.2017 21:13:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-as/ets-au-as-bsxets.xml"
14.12.2017 21:13:31 - "Statistics table: 0 ms"
14.12.2017 21:13:31 - "Test Suite 'Conformance class: Application schema, Administrative Units' started"
14.12.2017 21:13:31 - "Test Case 'Code list values' started"
14.12.2017 21:13:31 - "Test Assertion 'au-as.a.1: nationalLevel attributes': PASSED - 10 ms"
14.12.2017 21:13:31 - "Test Case 'Code list values' finished: PASSED"
14.12.2017 21:13:31 - "Test Case 'Constraints' started"
14.12.2017 21:13:31 - "Test Assertion 'au-as.b.1: No unit at highest level can associate units at a higher level': PASSED - 0 ms"
14.12.2017 21:13:31 - "Test Assertion 'au-as.b.2: No unit at lowest level can associate units at lower level': PASSED - 0 ms"
14.12.2017 21:13:31 - "Test Assertion 'au-as.b.3: Association role condominium applies only for administrative units which nationalLevel=1st order': PASSED - 0 ms"
14.12.2017 21:13:31 - "Test Case 'Constraints' finished: PASSED"
14.12.2017 21:13:31 - "Test Suite 'Conformance class: Application schema, Administrative Units' finished: PASSED"
14.12.2017 21:13:32 - Releasing resources
14.12.2017 21:13:32 - TestRunTask initialized
14.12.2017 21:13:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:32 - Validation ended with 0 error(s)
14.12.2017 21:13:32 - Compiling test script
14.12.2017 21:13:32 - Starting XQuery tests
14.12.2017 21:13:32 - "Testing 1 features"
14.12.2017 21:13:32 - "Indexing features (parsing errors: 0): 90 ms"
14.12.2017 21:13:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
14.12.2017 21:13:32 - "Statistics table: 0 ms"
14.12.2017 21:13:32 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
14.12.2017 21:13:32 - "Test Case 'Version consistency' started"
14.12.2017 21:13:32 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
14.12.2017 21:13:32 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
14.12.2017 21:13:32 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
14.12.2017 21:13:32 - "Test Case 'Temporal consistency' started"
14.12.2017 21:13:32 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Temporal consistency' finished: PASSED"
14.12.2017 21:13:32 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
14.12.2017 21:13:32 - Releasing resources
14.12.2017 21:13:32 - TestRunTask initialized
14.12.2017 21:13:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:32 - Validation ended with 0 error(s)
14.12.2017 21:13:32 - Compiling test script
14.12.2017 21:13:32 - Starting XQuery tests
14.12.2017 21:13:32 - "Testing 1 features"
14.12.2017 21:13:32 - "Indexing features (parsing errors: 0): 47 ms"
14.12.2017 21:13:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-dc/ets-au-dc-bsxets.xml"
14.12.2017 21:13:32 - "Statistics table: 0 ms"
14.12.2017 21:13:32 - "Test Suite 'Conformance class: Data consistency, Administrative Units' started"
14.12.2017 21:13:32 - "Test Case 'Higher Hierarchy Consistency' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.a.1: Each administrative unit refers to exactly one administrative unit at a higher level except those corresponding to the country level unit representing a Member State or being a co-administered unit.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Higher Hierarchy Consistency' finished: PASSED"
14.12.2017 21:13:32 - "Test Case 'Lower Hierarchy Consistency' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.b.1: Each administrative unit refers to a lower level administrative unit, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.b.2: Each administrative unit refers to lower level administrative units on the next level, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Lower Hierarchy Consistency' finished: PASSED"
14.12.2017 21:13:32 - "Test Case 'Co-administered' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.c.1: Each administrative unit co-administered by two or more administrative units uses the association role administeredBy, and the units co-administering this unit use the inverse role coAdminister.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Co-administered' finished: PASSED"
14.12.2017 21:13:32 - "Test Case 'Condominium administration' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.d.1: Each condominium is only administered by administrative units at country level.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Condominium administration' finished: PASSED"
14.12.2017 21:13:32 - "Test Case 'Area' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.e.1: Administrative units having the same level of administrative hierarchy do not conceptually share common areas.': PASSED - 130 ms"
14.12.2017 21:13:32 - "Test Case 'Area' finished: PASSED"
14.12.2017 21:13:32 - "Test Case 'Boundary' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.f.1: The geometry of each instance administrative boundary corresponds to an edge in the topological structure formed by the complete boundary graph, including the boundaries of all levels.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Boundary' finished: PASSED"
14.12.2017 21:13:32 - "Test Case 'Condominium spatial extent' started"
14.12.2017 21:13:32 - "Test Assertion 'au-dc.g.1: The spatial extent of a condominium is not part of the geometry representing the spatial extent of an administrative unit.': PASSED - 0 ms"
14.12.2017 21:13:32 - "Test Case 'Condominium spatial extent' finished: PASSED"
14.12.2017 21:13:32 - "Test Suite 'Conformance class: Data consistency, Administrative Units' finished: PASSED"
14.12.2017 21:13:33 - Releasing resources
14.12.2017 21:13:33 - TestRunTask initialized
14.12.2017 21:13:33 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:33 - Validation ended with 0 error(s)
14.12.2017 21:13:33 - Compiling test script
14.12.2017 21:13:33 - Starting XQuery tests
14.12.2017 21:13:33 - "Testing 1 features"
14.12.2017 21:13:33 - "Indexing features (parsing errors: 0): 36 ms"
14.12.2017 21:13:33 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
14.12.2017 21:13:33 - "Statistics table: 0 ms"
14.12.2017 21:13:33 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
14.12.2017 21:13:33 - "Test Case 'Coordinate reference systems (CRS)' started"
14.12.2017 21:13:33 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 0 ms"
14.12.2017 21:13:33 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
14.12.2017 21:13:33 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
14.12.2017 21:13:34 - Releasing resources
14.12.2017 21:13:34 - TestRunTask initialized
14.12.2017 21:13:34 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:13:34 - Validation ended with 0 error(s)
14.12.2017 21:13:34 - Compiling test script
14.12.2017 21:13:34 - Starting XQuery tests
14.12.2017 21:13:34 - "Testing 1 features"
14.12.2017 21:13:34 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-ia/ets-au-ia-bsxets.xml"
14.12.2017 21:13:34 - "Statistics table: 1 ms"
14.12.2017 21:13:34 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' started"
14.12.2017 21:13:34 - "Test Case 'Code lists' started"
14.12.2017 21:13:34 - "Test Assertion 'au-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
14.12.2017 21:13:34 - "Test Case 'Code lists' finished: PASSED"
14.12.2017 21:13:34 - "Test Case 'Feature references' started"
14.12.2017 21:13:34 - "Test Assertion 'au-ia.b.1: AdministrativeUnit.NUTS': PASSED - 0 ms"
14.12.2017 21:13:34 - "Test Assertion 'au-ia.b.2: AdministrativeUnit.Condominium': PASSED - 0 ms"
14.12.2017 21:13:34 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmUnitS.14700'"
14.12.2017 21:14:04 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmUnitS.14700"
14.12.2017 21:14:04 - "Test Assertion 'au-ia.b.3: AdministrativeUnit.upperLevelUnit': FAILED - 30017 ms"
14.12.2017 21:14:04 - "Test Assertion 'au-ia.b.4: AdministrativeUnit.lowerLevelUnit': PASSED - 0 ms"
14.12.2017 21:14:04 - "Test Assertion 'au-ia.b.5: AdministrativeUnit.administeredBy': PASSED - 0 ms"
14.12.2017 21:14:04 - "Test Assertion 'au-ia.b.6: AdministrativeUnit.coAdminister': PASSED - 0 ms"
14.12.2017 21:14:04 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1143'"
14.12.2017 21:14:34 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1143"
14.12.2017 21:14:34 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1236'"
14.12.2017 21:15:05 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1236"
14.12.2017 21:15:05 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1332'"
14.12.2017 21:15:35 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1332"
14.12.2017 21:15:35 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1419'"
14.12.2017 21:16:05 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1419"
14.12.2017 21:16:05 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1424'"
14.12.2017 21:16:35 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1424"
14.12.2017 21:16:35 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1507'"
14.12.2017 21:17:05 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1507"
14.12.2017 21:17:05 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1646'"
14.12.2017 21:17:35 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.1646"
14.12.2017 21:17:35 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2522'"
14.12.2017 21:18:05 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2522"
14.12.2017 21:18:05 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2554'"
14.12.2017 21:18:35 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2554"
14.12.2017 21:18:35 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2555'"
14.12.2017 21:19:06 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2555"
14.12.2017 21:19:06 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2611'"
14.12.2017 21:19:36 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2611"
14.12.2017 21:19:36 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2836'"
14.12.2017 21:20:06 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2836"
14.12.2017 21:20:06 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2921'"
14.12.2017 21:20:36 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2921"
14.12.2017 21:20:36 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2995'"
14.12.2017 21:21:06 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.2995"
14.12.2017 21:21:06 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.3115'"
14.12.2017 21:21:36 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.3115"
14.12.2017 21:21:36 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.3161'"
14.12.2017 21:22:06 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.3161"
14.12.2017 21:22:06 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.3194'"
14.12.2017 21:22:36 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=auAdmBoundaryL.3194"
14.12.2017 21:22:36 - "Test Assertion 'au-ia.b.7: AdministrativeUnit.boundary': FAILED - 512022 ms"
14.12.2017 21:22:36 - "Test Assertion 'au-ia.b.8: AdministrativeBoundary.admUnit and Condominium.admUnit': PASSED - 0 ms"
14.12.2017 21:22:36 - "Test Assertion 'au-ia.b.9: MaritimeZone.boundary': PASSED - 0 ms"
14.12.2017 21:22:36 - "Test Assertion 'au-ia.b.10: MaritimeZone.seaArea': PASSED - 0 ms"
14.12.2017 21:22:36 - "Test Assertion 'au-ia.b.11: MaritimeZone.baseline': PASSED - 1 ms"
14.12.2017 21:22:36 - "Test Case 'Feature references' finished: FAILED"
14.12.2017 21:22:36 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' finished: FAILED"
14.12.2017 21:22:37 - Releasing resources
14.12.2017 21:22:37 - TestRunTask initialized
14.12.2017 21:22:37 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:22:37 - Validation ended with 0 error(s)
14.12.2017 21:22:37 - Compiling test script
14.12.2017 21:22:37 - Starting XQuery tests
14.12.2017 21:22:37 - "Testing 1 features"
14.12.2017 21:22:37 - "Indexing features (parsing errors: 0): 39 ms"
14.12.2017 21:22:37 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
14.12.2017 21:22:37 - "Statistics table: 1 ms"
14.12.2017 21:22:37 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
14.12.2017 21:22:37 - "Test Case 'Spatial reference systems' started"
14.12.2017 21:22:37 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
14.12.2017 21:22:37 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 1 ms"
14.12.2017 21:22:37 - "Test Case 'Spatial reference systems' finished: FAILED"
14.12.2017 21:22:37 - "Test Case 'Temporal reference systems' started"
14.12.2017 21:22:37 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
14.12.2017 21:22:37 - "Test Case 'Temporal reference systems' finished: PASSED"
14.12.2017 21:22:37 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
14.12.2017 21:22:38 - Releasing resources
14.12.2017 21:22:38 - TestRunTask initialized
14.12.2017 21:22:38 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:22:38 - Validation ended with 0 error(s)
14.12.2017 21:22:38 - Compiling test script
14.12.2017 21:22:38 - Starting XQuery tests
14.12.2017 21:22:38 - "Testing 1 features"
14.12.2017 21:22:38 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-rs/ets-au-rs-bsxets.xml"
14.12.2017 21:22:38 - "Statistics table: 1 ms"
14.12.2017 21:22:38 - "Test Suite 'Conformance class: Reference systems, Administrative Units' started"
14.12.2017 21:22:38 - "Test Case 'Additional theme-specific rules for reference systems' started"
14.12.2017 21:22:38 - "Test Assertion 'au-rs.a.1: Test always passes': PASSED - 0 ms"
14.12.2017 21:22:38 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
14.12.2017 21:22:38 - "Test Suite 'Conformance class: Reference systems, Administrative Units' finished: PASSED"
14.12.2017 21:22:38 - Releasing resources
14.12.2017 21:22:38 - Changed state from INITIALIZED to RUNNING
14.12.2017 21:22:38 - Duration: 9min
14.12.2017 21:22:38 - TestRun finished
14.12.2017 21:22:38 - Changed state from RUNNING to COMPLETED
