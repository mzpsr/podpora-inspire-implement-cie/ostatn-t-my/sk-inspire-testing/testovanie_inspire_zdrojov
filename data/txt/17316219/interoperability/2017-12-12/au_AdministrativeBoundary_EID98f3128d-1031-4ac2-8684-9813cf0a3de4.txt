12.12.2017 02:27:41 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_au:AdministrativeBoundary (initiated Tue Dec 12 02:27:41 CET 2017)
12.12.2017 02:27:41 - Resolving Executable Test Suite dependencies
12.12.2017 02:27:41 - Preparing 10 Test Task:
12.12.2017 02:27:41 -  TestTask 1 (f17af593-ad4c-4ac0-be88-adf41f85cc4c)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 2 (d7514c0f-7bd4-405c-8e55-4358e7b7fbe5)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.09820daf-62b2-4fa3-a95f-56a0d2b7c4d8'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 3 (5a5aa007-7afe-4cdc-a135-b22c1647c609)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.47c569bc-677d-4ce3-8411-e2b29189332a'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 4 (c00c6746-088a-4e2a-b8a5-e3fb6430ca20)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Administrative Units (EID: ddecef4b-89a3-4f9d-9246-a50b588fa5a2, V: 0.2.1 )'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 5 (91881aa0-f14b-4fd5-84d3-2d760ef34934)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 6 (f43e3a76-e852-4883-9c91-f5d583b05707)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Administrative Units (EID: acc5931c-4ff0-499f-b916-3cda1603456b, V: 0.2.2 )'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 7 (0cf35ed4-8bd0-401c-a53b-f5c1e08c9850)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 8 (e8662ab8-cbc9-42c8-84cd-a89c4d1cdba2)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Administrative Units (EID: ee28b75e-5c80-4370-838d-ab1b18e30b13, V: 0.2.1 )'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 9 (912345c6-0561-4fc9-b76e-96df4cf36a0a)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 -  TestTask 10 (8bb94908-48a2-4c44-8c98-7e443d0c5119)
12.12.2017 02:27:41 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Administrative Units (EID: cafb75f8-5deb-4cca-89df-d3189322e97f, V: 0.2.0 )'
12.12.2017 02:27:41 -  with parameters: 
12.12.2017 02:27:41 - etf.testcases = *
12.12.2017 02:27:41 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 02:27:41 - Setting state to CREATED
12.12.2017 02:27:41 - Changed state from CREATED to INITIALIZING
12.12.2017 02:27:41 - Starting TestRun.98f3128d-1031-4ac2-8684-9813cf0a3de4 at 2017-12-12T02:27:43+01:00
12.12.2017 02:27:43 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 02:27:43 - TestRunTask initialized
12.12.2017 02:27:43 - Creating new tests databases to speed up tests.
12.12.2017 02:27:43 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:43 - Optimizing last database etf-tdb-5c3e3925-986d-4040-a8ad-f376788ff876-0 
12.12.2017 02:27:43 - Import completed
12.12.2017 02:27:44 - Validation ended with 0 error(s)
12.12.2017 02:27:44 - Compiling test script
12.12.2017 02:27:44 - Starting XQuery tests
12.12.2017 02:27:44 - "Testing 1 features"
12.12.2017 02:27:44 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
12.12.2017 02:27:44 - "Statistics table: 0 ms"
12.12.2017 02:27:44 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
12.12.2017 02:27:44 - "Test Case 'Basic tests' started"
12.12.2017 02:27:44 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
12.12.2017 02:27:44 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
12.12.2017 02:27:44 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
12.12.2017 02:27:44 - "Test Case 'Basic tests' finished: PASSED"
12.12.2017 02:27:44 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
12.12.2017 02:27:44 - Releasing resources
12.12.2017 02:27:44 - TestRunTask initialized
12.12.2017 02:27:44 - Recreating new tests databases as the Test Object has changed!
12.12.2017 02:27:44 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:44 - Optimizing last database etf-tdb-5c3e3925-986d-4040-a8ad-f376788ff876-0 
12.12.2017 02:27:44 - Import completed
12.12.2017 02:27:44 - Validation ended with 0 error(s)
12.12.2017 02:27:44 - Compiling test script
12.12.2017 02:27:44 - Starting XQuery tests
12.12.2017 02:27:44 - "Testing 1 features"
12.12.2017 02:27:44 - "Indexing features (parsing errors: 0): 40 ms"
12.12.2017 02:27:44 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
12.12.2017 02:27:44 - "Statistics table: 1 ms"
12.12.2017 02:27:44 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
12.12.2017 02:27:44 - "Test Case 'Schema' started"
12.12.2017 02:27:44 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
12.12.2017 02:27:44 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
12.12.2017 02:27:44 - "Test Case 'Schema' finished: PASSED_MANUAL"
12.12.2017 02:27:44 - "Test Case 'Schema validation' started"
12.12.2017 02:27:44 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
12.12.2017 02:27:44 - "Validating get.xml"
12.12.2017 02:27:50 - "Duration: 5927 ms. Errors: 0."
12.12.2017 02:27:50 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 5927 ms"
12.12.2017 02:27:50 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 02:27:50 - "Test Case 'GML model' started"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Case 'GML model' finished: PASSED"
12.12.2017 02:27:50 - "Test Case 'Simple features' started"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 1 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 45 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Case 'Simple features' finished: PASSED"
12.12.2017 02:27:50 - "Test Case 'Code list values in basic data types' started"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 8 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 4 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
12.12.2017 02:27:50 - "Test Case 'Code list values in basic data types' finished: PASSED"
12.12.2017 02:27:50 - "Test Case 'Constraints' started"
12.12.2017 02:27:50 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Case 'Constraints' finished: PASSED"
12.12.2017 02:27:50 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
12.12.2017 02:27:50 - Releasing resources
12.12.2017 02:27:50 - TestRunTask initialized
12.12.2017 02:27:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:50 - Validation ended with 0 error(s)
12.12.2017 02:27:50 - Compiling test script
12.12.2017 02:27:50 - Starting XQuery tests
12.12.2017 02:27:50 - "Testing 1 features"
12.12.2017 02:27:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-gml/ets-au-gml-bsxets.xml"
12.12.2017 02:27:50 - "Statistics table: 0 ms"
12.12.2017 02:27:50 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' started"
12.12.2017 02:27:50 - "Test Case 'Basic test' started"
12.12.2017 02:27:50 - "Test Assertion 'au-gml.a.1: Administrative Unit feature in dataset': PASSED - 0 ms"
12.12.2017 02:27:50 - "Test Case 'Basic test' finished: PASSED"
12.12.2017 02:27:50 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' finished: PASSED"
12.12.2017 02:27:50 - Releasing resources
12.12.2017 02:27:50 - TestRunTask initialized
12.12.2017 02:27:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:51 - Validation ended with 0 error(s)
12.12.2017 02:27:51 - Compiling test script
12.12.2017 02:27:51 - Starting XQuery tests
12.12.2017 02:27:51 - "Testing 1 features"
12.12.2017 02:27:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-as/ets-au-as-bsxets.xml"
12.12.2017 02:27:51 - "Statistics table: 0 ms"
12.12.2017 02:27:51 - "Test Suite 'Conformance class: Application schema, Administrative Units' started"
12.12.2017 02:27:51 - "Test Case 'Code list values' started"
12.12.2017 02:27:51 - "Test Assertion 'au-as.a.1: nationalLevel attributes': PASSED - 9 ms"
12.12.2017 02:27:51 - "Test Case 'Code list values' finished: PASSED"
12.12.2017 02:27:51 - "Test Case 'Constraints' started"
12.12.2017 02:27:51 - "Test Assertion 'au-as.b.1: No unit at highest level can associate units at a higher level': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Assertion 'au-as.b.2: No unit at lowest level can associate units at lower level': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Assertion 'au-as.b.3: Association role condominium applies only for administrative units which nationalLevel=1st order': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Constraints' finished: PASSED"
12.12.2017 02:27:51 - "Test Suite 'Conformance class: Application schema, Administrative Units' finished: PASSED"
12.12.2017 02:27:51 - Releasing resources
12.12.2017 02:27:51 - TestRunTask initialized
12.12.2017 02:27:51 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:51 - Validation ended with 0 error(s)
12.12.2017 02:27:51 - Compiling test script
12.12.2017 02:27:51 - Starting XQuery tests
12.12.2017 02:27:51 - "Testing 1 features"
12.12.2017 02:27:51 - "Indexing features (parsing errors: 0): 38 ms"
12.12.2017 02:27:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
12.12.2017 02:27:51 - "Statistics table: 1 ms"
12.12.2017 02:27:51 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
12.12.2017 02:27:51 - "Test Case 'Version consistency' started"
12.12.2017 02:27:51 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
12.12.2017 02:27:51 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
12.12.2017 02:27:51 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
12.12.2017 02:27:51 - "Test Case 'Temporal consistency' started"
12.12.2017 02:27:51 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Temporal consistency' finished: PASSED"
12.12.2017 02:27:51 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
12.12.2017 02:27:51 - Releasing resources
12.12.2017 02:27:51 - TestRunTask initialized
12.12.2017 02:27:51 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:51 - Validation ended with 0 error(s)
12.12.2017 02:27:51 - Compiling test script
12.12.2017 02:27:51 - Starting XQuery tests
12.12.2017 02:27:51 - "Testing 1 features"
12.12.2017 02:27:51 - "Indexing features (parsing errors: 0): 40 ms"
12.12.2017 02:27:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-dc/ets-au-dc-bsxets.xml"
12.12.2017 02:27:51 - "Statistics table: 0 ms"
12.12.2017 02:27:51 - "Test Suite 'Conformance class: Data consistency, Administrative Units' started"
12.12.2017 02:27:51 - "Test Case 'Higher Hierarchy Consistency' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.a.1: Each administrative unit refers to exactly one administrative unit at a higher level except those corresponding to the country level unit representing a Member State or being a co-administered unit.': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Higher Hierarchy Consistency' finished: PASSED"
12.12.2017 02:27:51 - "Test Case 'Lower Hierarchy Consistency' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.b.1: Each administrative unit refers to a lower level administrative unit, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.b.2: Each administrative unit refers to lower level administrative units on the next level, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Lower Hierarchy Consistency' finished: PASSED"
12.12.2017 02:27:51 - "Test Case 'Co-administered' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.c.1: Each administrative unit co-administered by two or more administrative units uses the association role administeredBy, and the units co-administering this unit use the inverse role coAdminister.': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Co-administered' finished: PASSED"
12.12.2017 02:27:51 - "Test Case 'Condominium administration' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.d.1: Each condominium is only administered by administrative units at country level.': PASSED - 1 ms"
12.12.2017 02:27:51 - "Test Case 'Condominium administration' finished: PASSED"
12.12.2017 02:27:51 - "Test Case 'Area' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.e.1: Administrative units having the same level of administrative hierarchy do not conceptually share common areas.': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Area' finished: PASSED"
12.12.2017 02:27:51 - "Test Case 'Boundary' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.f.1: The geometry of each instance administrative boundary corresponds to an edge in the topological structure formed by the complete boundary graph, including the boundaries of all levels.': FAILED - 80 ms"
12.12.2017 02:27:51 - "Test Case 'Boundary' finished: FAILED"
12.12.2017 02:27:51 - "Test Case 'Condominium spatial extent' started"
12.12.2017 02:27:51 - "Test Assertion 'au-dc.g.1: The spatial extent of a condominium is not part of the geometry representing the spatial extent of an administrative unit.': PASSED - 0 ms"
12.12.2017 02:27:51 - "Test Case 'Condominium spatial extent' finished: PASSED"
12.12.2017 02:27:51 - "Test Suite 'Conformance class: Data consistency, Administrative Units' finished: FAILED"
12.12.2017 02:27:52 - Releasing resources
12.12.2017 02:27:52 - TestRunTask initialized
12.12.2017 02:27:52 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:52 - Validation ended with 0 error(s)
12.12.2017 02:27:52 - Compiling test script
12.12.2017 02:27:52 - Starting XQuery tests
12.12.2017 02:27:52 - "Testing 1 features"
12.12.2017 02:27:52 - "Indexing features (parsing errors: 0): 71 ms"
12.12.2017 02:27:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
12.12.2017 02:27:52 - "Statistics table: 1 ms"
12.12.2017 02:27:52 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
12.12.2017 02:27:52 - "Test Case 'Coordinate reference systems (CRS)' started"
12.12.2017 02:27:52 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
12.12.2017 02:27:52 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
12.12.2017 02:27:52 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
12.12.2017 02:27:52 - Releasing resources
12.12.2017 02:27:52 - TestRunTask initialized
12.12.2017 02:27:52 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:27:52 - Validation ended with 0 error(s)
12.12.2017 02:27:52 - Compiling test script
12.12.2017 02:27:52 - Starting XQuery tests
12.12.2017 02:27:52 - "Testing 1 features"
12.12.2017 02:27:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-ia/ets-au-ia-bsxets.xml"
12.12.2017 02:27:52 - "Statistics table: 0 ms"
12.12.2017 02:27:52 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' started"
12.12.2017 02:27:52 - "Test Case 'Code lists' started"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
12.12.2017 02:27:52 - "Test Case 'Code lists' finished: PASSED"
12.12.2017 02:27:52 - "Test Case 'Feature references' started"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.1: AdministrativeUnit.NUTS': PASSED - 0 ms"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.2: AdministrativeUnit.Condominium': PASSED - 0 ms"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.3: AdministrativeUnit.upperLevelUnit': PASSED - 0 ms"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.4: AdministrativeUnit.lowerLevelUnit': PASSED - 0 ms"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.5: AdministrativeUnit.administeredBy': PASSED - 0 ms"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.6: AdministrativeUnit.coAdminister': PASSED - 1 ms"
12.12.2017 02:27:52 - "Test Assertion 'au-ia.b.7: AdministrativeUnit.boundary': PASSED - 0 ms"
12.12.2017 02:27:52 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=http://inspire.ec.europa.eu/codelist/Admi..."
12.12.2017 02:28:22 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=http://inspire.ec.euro..."
12.12.2017 02:28:22 - "Test Assertion 'au-ia.b.8: AdministrativeBoundary.admUnit and Condominium.admUnit': FAILED - 30029 ms"
12.12.2017 02:28:22 - "Test Assertion 'au-ia.b.9: MaritimeZone.boundary': PASSED - 0 ms"
12.12.2017 02:28:22 - "Test Assertion 'au-ia.b.10: MaritimeZone.seaArea': PASSED - 0 ms"
12.12.2017 02:28:22 - "Test Assertion 'au-ia.b.11: MaritimeZone.baseline': PASSED - 0 ms"
12.12.2017 02:28:22 - "Test Case 'Feature references' finished: FAILED"
12.12.2017 02:28:22 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' finished: FAILED"
12.12.2017 02:28:23 - Releasing resources
12.12.2017 02:28:23 - TestRunTask initialized
12.12.2017 02:28:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:28:23 - Validation ended with 0 error(s)
12.12.2017 02:28:23 - Compiling test script
12.12.2017 02:28:23 - Starting XQuery tests
12.12.2017 02:28:23 - "Testing 1 features"
12.12.2017 02:28:23 - "Indexing features (parsing errors: 0): 38 ms"
12.12.2017 02:28:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
12.12.2017 02:28:23 - "Statistics table: 0 ms"
12.12.2017 02:28:23 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
12.12.2017 02:28:23 - "Test Case 'Spatial reference systems' started"
12.12.2017 02:28:23 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
12.12.2017 02:28:23 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
12.12.2017 02:28:23 - "Test Case 'Spatial reference systems' finished: FAILED"
12.12.2017 02:28:23 - "Test Case 'Temporal reference systems' started"
12.12.2017 02:28:23 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
12.12.2017 02:28:23 - "Test Case 'Temporal reference systems' finished: PASSED"
12.12.2017 02:28:23 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
12.12.2017 02:28:23 - Releasing resources
12.12.2017 02:28:23 - TestRunTask initialized
12.12.2017 02:28:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:28:23 - Validation ended with 0 error(s)
12.12.2017 02:28:23 - Compiling test script
12.12.2017 02:28:23 - Starting XQuery tests
12.12.2017 02:28:23 - "Testing 1 features"
12.12.2017 02:28:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-rs/ets-au-rs-bsxets.xml"
12.12.2017 02:28:23 - "Statistics table: 0 ms"
12.12.2017 02:28:23 - "Test Suite 'Conformance class: Reference systems, Administrative Units' started"
12.12.2017 02:28:23 - "Test Case 'Additional theme-specific rules for reference systems' started"
12.12.2017 02:28:23 - "Test Assertion 'au-rs.a.1: Test always passes': PASSED - 0 ms"
12.12.2017 02:28:23 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
12.12.2017 02:28:23 - "Test Suite 'Conformance class: Reference systems, Administrative Units' finished: PASSED"
12.12.2017 02:28:23 - Releasing resources
12.12.2017 02:28:23 - Changed state from INITIALIZED to RUNNING
12.12.2017 02:28:23 - Duration: 42sec
12.12.2017 02:28:23 - TestRun finished
12.12.2017 02:28:23 - Changed state from RUNNING to COMPLETED
