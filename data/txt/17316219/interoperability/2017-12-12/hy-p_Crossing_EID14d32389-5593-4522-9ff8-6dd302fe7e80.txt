12.12.2017 09:00:55 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_hy-p:Crossing (initiated Tue Dec 12 09:00:55 CET 2017)
12.12.2017 09:00:55 - Resolving Executable Test Suite dependencies
12.12.2017 09:00:55 - Preparing 10 Test Task:
12.12.2017 09:00:55 -  TestTask 1 (4bd24cdf-ecdc-40bf-913e-aabec69685dd)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 2 (668c8f43-bcf8-44b6-b3d0-ce63e735ef0b)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 3 (0f660c1e-5013-41a8-868b-68d25b8f59e9)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 4 (3d4b7686-f66a-4c6d-a253-e5e373e9aac0)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 5 (ebe891d9-6e2e-4e4f-9a1a-6ea41c740997)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 6 (c01bbfcd-6665-43c7-bfa8-9e4c594e09e4)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 7 (fc665880-e181-42ea-8eca-a711c38b9c38)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 8 (8e466347-28aa-48f1-afc8-1313e8ae6b43)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 9 (41b660ce-b239-4564-8118-15bd66dcd083)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 -  TestTask 10 (385de43d-b767-4e84-9101-f9dd5e2aef8c)
12.12.2017 09:00:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
12.12.2017 09:00:55 -  with parameters: 
12.12.2017 09:00:55 - etf.testcases = *
12.12.2017 09:00:55 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 09:00:55 - Setting state to CREATED
12.12.2017 09:00:55 - Changed state from CREATED to INITIALIZING
12.12.2017 09:00:55 - Starting TestRun.14d32389-5593-4522-9ff8-6dd302fe7e80 at 2017-12-12T09:00:56+01:00
12.12.2017 09:00:56 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 09:00:56 - TestRunTask initialized
12.12.2017 09:00:56 - Creating new tests databases to speed up tests.
12.12.2017 09:00:56 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:00:56 - Optimizing last database etf-tdb-04f87be8-4899-41c4-a1fa-2f7855b24b8f-0 
12.12.2017 09:00:56 - Import completed
12.12.2017 09:00:58 - Validation ended with 0 error(s)
12.12.2017 09:00:58 - Compiling test script
12.12.2017 09:00:58 - Starting XQuery tests
12.12.2017 09:00:58 - "Testing 1 features"
12.12.2017 09:00:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
12.12.2017 09:00:58 - "Statistics table: 0 ms"
12.12.2017 09:00:58 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
12.12.2017 09:00:58 - "Test Case 'Basic tests' started"
12.12.2017 09:00:58 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
12.12.2017 09:00:58 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
12.12.2017 09:00:58 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
12.12.2017 09:00:58 - "Test Case 'Basic tests' finished: PASSED"
12.12.2017 09:00:58 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
12.12.2017 09:00:58 - Releasing resources
12.12.2017 09:00:58 - TestRunTask initialized
12.12.2017 09:00:58 - Recreating new tests databases as the Test Object has changed!
12.12.2017 09:00:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:00:58 - Optimizing last database etf-tdb-04f87be8-4899-41c4-a1fa-2f7855b24b8f-0 
12.12.2017 09:00:58 - Import completed
12.12.2017 09:00:58 - Validation ended with 0 error(s)
12.12.2017 09:00:58 - Compiling test script
12.12.2017 09:00:58 - Starting XQuery tests
12.12.2017 09:00:58 - "Testing 1 features"
12.12.2017 09:00:58 - "Indexing features (parsing errors: 0): 42 ms"
12.12.2017 09:00:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
12.12.2017 09:00:58 - "Statistics table: 1 ms"
12.12.2017 09:00:58 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
12.12.2017 09:00:58 - "Test Case 'Schema' started"
12.12.2017 09:00:58 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
12.12.2017 09:00:58 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
12.12.2017 09:00:58 - "Test Case 'Schema' finished: PASSED_MANUAL"
12.12.2017 09:00:58 - "Test Case 'Schema validation' started"
12.12.2017 09:00:58 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
12.12.2017 09:00:58 - "Validating get.xml"
12.12.2017 09:01:06 - "Duration: 7529 ms. Errors: 0."
12.12.2017 09:01:06 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 7530 ms"
12.12.2017 09:01:06 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 09:01:06 - "Test Case 'GML model' started"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Case 'GML model' finished: PASSED"
12.12.2017 09:01:06 - "Test Case 'Simple features' started"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 1 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 39 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Case 'Simple features' finished: PASSED"
12.12.2017 09:01:06 - "Test Case 'Code list values in basic data types' started"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 67 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 10 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 6 ms"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 13 ms"
12.12.2017 09:01:06 - "Test Case 'Code list values in basic data types' finished: PASSED"
12.12.2017 09:01:06 - "Test Case 'Constraints' started"
12.12.2017 09:01:06 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Case 'Constraints' finished: PASSED"
12.12.2017 09:01:06 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
12.12.2017 09:01:06 - Releasing resources
12.12.2017 09:01:06 - TestRunTask initialized
12.12.2017 09:01:06 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:06 - Validation ended with 0 error(s)
12.12.2017 09:01:06 - Compiling test script
12.12.2017 09:01:06 - Starting XQuery tests
12.12.2017 09:01:06 - "Testing 1 features"
12.12.2017 09:01:06 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
12.12.2017 09:01:06 - "Statistics table: 1 ms"
12.12.2017 09:01:06 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
12.12.2017 09:01:06 - "Test Case 'Basic test' started"
12.12.2017 09:01:06 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Case 'Basic test' finished: PASSED"
12.12.2017 09:01:06 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: PASSED"
12.12.2017 09:01:06 - Releasing resources
12.12.2017 09:01:06 - TestRunTask initialized
12.12.2017 09:01:06 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:06 - Validation ended with 0 error(s)
12.12.2017 09:01:06 - Compiling test script
12.12.2017 09:01:06 - Starting XQuery tests
12.12.2017 09:01:06 - "Testing 1 features"
12.12.2017 09:01:06 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
12.12.2017 09:01:06 - "Statistics table: 0 ms"
12.12.2017 09:01:06 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
12.12.2017 09:01:06 - "Test Case 'Code list values' started"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 5 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.a.2: type attributes': FAILED - 4 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 6 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 3 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 3 ms"
12.12.2017 09:01:06 - "Test Case 'Code list values' finished: FAILED"
12.12.2017 09:01:06 - "Test Case 'Geometry' started"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
12.12.2017 09:01:06 - "Test Case 'Geometry' finished: PASSED_MANUAL"
12.12.2017 09:01:06 - "Test Case 'Identifiers and references' started"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
12.12.2017 09:01:06 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
12.12.2017 09:01:06 - "Test Case 'Constraints' started"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
12.12.2017 09:01:06 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
12.12.2017 09:01:06 - "Test Case 'Constraints' finished: PASSED_MANUAL"
12.12.2017 09:01:06 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: FAILED"
12.12.2017 09:01:07 - Releasing resources
12.12.2017 09:01:07 - TestRunTask initialized
12.12.2017 09:01:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:07 - Validation ended with 0 error(s)
12.12.2017 09:01:07 - Compiling test script
12.12.2017 09:01:07 - Starting XQuery tests
12.12.2017 09:01:07 - "Testing 1 features"
12.12.2017 09:01:07 - "Indexing features (parsing errors: 0): 34 ms"
12.12.2017 09:01:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
12.12.2017 09:01:07 - "Statistics table: 0 ms"
12.12.2017 09:01:07 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
12.12.2017 09:01:07 - "Test Case 'Version consistency' started"
12.12.2017 09:01:07 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
12.12.2017 09:01:07 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Temporal consistency' started"
12.12.2017 09:01:07 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
12.12.2017 09:01:07 - "Test Case 'Temporal consistency' finished: PASSED"
12.12.2017 09:01:07 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
12.12.2017 09:01:07 - Releasing resources
12.12.2017 09:01:07 - TestRunTask initialized
12.12.2017 09:01:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:07 - Validation ended with 0 error(s)
12.12.2017 09:01:07 - Compiling test script
12.12.2017 09:01:07 - Starting XQuery tests
12.12.2017 09:01:07 - "Testing 1 features"
12.12.2017 09:01:07 - "Indexing features (parsing errors: 0): 35 ms"
12.12.2017 09:01:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
12.12.2017 09:01:07 - "Statistics table: 0 ms"
12.12.2017 09:01:07 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
12.12.2017 09:01:07 - "Test Case 'Spatial consistency' started"
12.12.2017 09:01:07 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 1 ms"
12.12.2017 09:01:07 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Thematic consistency' started"
12.12.2017 09:01:07 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Identifiers' started"
12.12.2017 09:01:07 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
12.12.2017 09:01:07 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
12.12.2017 09:01:07 - Releasing resources
12.12.2017 09:01:07 - TestRunTask initialized
12.12.2017 09:01:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:07 - Validation ended with 0 error(s)
12.12.2017 09:01:07 - Compiling test script
12.12.2017 09:01:07 - Starting XQuery tests
12.12.2017 09:01:07 - "Testing 1 features"
12.12.2017 09:01:07 - "Indexing features (parsing errors: 0): 34 ms"
12.12.2017 09:01:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
12.12.2017 09:01:07 - "Statistics table: 0 ms"
12.12.2017 09:01:07 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
12.12.2017 09:01:07 - "Test Case 'Coordinate reference systems (CRS)' started"
12.12.2017 09:01:07 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
12.12.2017 09:01:07 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
12.12.2017 09:01:07 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
12.12.2017 09:01:08 - Releasing resources
12.12.2017 09:01:08 - TestRunTask initialized
12.12.2017 09:01:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:08 - Validation ended with 0 error(s)
12.12.2017 09:01:08 - Compiling test script
12.12.2017 09:01:08 - Starting XQuery tests
12.12.2017 09:01:08 - "Testing 1 features"
12.12.2017 09:01:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
12.12.2017 09:01:08 - "Statistics table: 1 ms"
12.12.2017 09:01:08 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
12.12.2017 09:01:08 - "Test Case 'Code lists' started"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Case 'Code lists' finished: PASSED"
12.12.2017 09:01:08 - "Test Case 'Feature references' started"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Case 'Feature references' finished: PASSED"
12.12.2017 09:01:08 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
12.12.2017 09:01:08 - Releasing resources
12.12.2017 09:01:08 - TestRunTask initialized
12.12.2017 09:01:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:08 - Validation ended with 0 error(s)
12.12.2017 09:01:08 - Compiling test script
12.12.2017 09:01:08 - Starting XQuery tests
12.12.2017 09:01:08 - "Testing 1 features"
12.12.2017 09:01:08 - "Indexing features (parsing errors: 0): 36 ms"
12.12.2017 09:01:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
12.12.2017 09:01:08 - "Statistics table: 1 ms"
12.12.2017 09:01:08 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
12.12.2017 09:01:08 - "Test Case 'Spatial reference systems' started"
12.12.2017 09:01:08 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Case 'Spatial reference systems' finished: FAILED"
12.12.2017 09:01:08 - "Test Case 'Temporal reference systems' started"
12.12.2017 09:01:08 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Case 'Temporal reference systems' finished: PASSED"
12.12.2017 09:01:08 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
12.12.2017 09:01:08 - Releasing resources
12.12.2017 09:01:08 - TestRunTask initialized
12.12.2017 09:01:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 09:01:08 - Validation ended with 0 error(s)
12.12.2017 09:01:08 - Compiling test script
12.12.2017 09:01:08 - Starting XQuery tests
12.12.2017 09:01:08 - "Testing 1 features"
12.12.2017 09:01:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
12.12.2017 09:01:08 - "Statistics table: 0 ms"
12.12.2017 09:01:08 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
12.12.2017 09:01:08 - "Test Case 'Units of measure' started"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
12.12.2017 09:01:08 - "Test Case 'Units of measure' finished: PASSED"
12.12.2017 09:01:08 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
12.12.2017 09:01:09 - Releasing resources
12.12.2017 09:01:09 - Changed state from INITIALIZED to RUNNING
12.12.2017 09:01:09 - Duration: 13sec
12.12.2017 09:01:09 - TestRun finished
12.12.2017 09:01:09 - Changed state from RUNNING to COMPLETED
