12.12.2017 02:40:56 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_INSPIRE - Dopravn&eacute; siete (initiated Tue Dec 12 02:40:56 CET 2017)
12.12.2017 02:40:56 - Resolving Executable Test Suite dependencies
12.12.2017 02:40:56 - Preparing 2 Test Task:
12.12.2017 02:40:56 -  TestTask 1 (e77e6825-e0a6-42a1-92ee-1359bcda9ea6)
12.12.2017 02:40:56 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.12.2017 02:40:56 -  with parameters: 
12.12.2017 02:40:56 - etf.testcases = *
12.12.2017 02:40:56 -  TestTask 2 (7aa7fb5b-7f4c-41ed-83ce-4aad7d52be8a)
12.12.2017 02:40:56 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
12.12.2017 02:40:56 -  with parameters: 
12.12.2017 02:40:56 - etf.testcases = *
12.12.2017 02:40:56 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 02:40:56 - Setting state to CREATED
12.12.2017 02:40:56 - Changed state from CREATED to INITIALIZING
12.12.2017 02:40:56 - Starting TestRun.a1f81878-8dbd-48d8-9135-9f96bd7b05f4 at 2017-12-12T02:40:57+01:00
12.12.2017 02:40:57 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 02:40:57 - TestRunTask initialized
12.12.2017 02:40:57 - Creating new tests databases to speed up tests.
12.12.2017 02:40:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:40:57 - Optimizing last database etf-tdb-004991fb-213d-4c2a-ad18-63175656ca23-0 
12.12.2017 02:40:57 - Import completed
12.12.2017 02:41:04 - Validation ended with 0 error(s)
12.12.2017 02:41:04 - Compiling test script
12.12.2017 02:41:04 - Starting XQuery tests
12.12.2017 02:41:04 - "Testing 1 records"
12.12.2017 02:41:04 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.12.2017 02:41:04 - "Statistics table: 0 ms"
12.12.2017 02:41:04 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.12.2017 02:41:04 - "Test Case 'Schema validation' started"
12.12.2017 02:41:06 - "Validating file GetRecordByIdResponse.xml: 2189 ms"
12.12.2017 02:41:06 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2189 ms"
12.12.2017 02:41:06 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 02:41:06 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
12.12.2017 02:41:12 - Releasing resources
12.12.2017 02:41:12 - TestRunTask initialized
12.12.2017 02:41:12 - Recreating new tests databases as the Test Object has changed!
12.12.2017 02:41:12 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:41:12 - Optimizing last database etf-tdb-004991fb-213d-4c2a-ad18-63175656ca23-0 
12.12.2017 02:41:12 - Import completed
12.12.2017 02:41:12 - Validation ended with 0 error(s)
12.12.2017 02:41:13 - Compiling test script
12.12.2017 02:41:13 - Starting XQuery tests
12.12.2017 02:41:13 - "Testing 1 records"
12.12.2017 02:41:13 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.12.2017 02:41:13 - "Statistics table: 1 ms"
12.12.2017 02:41:13 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.12.2017 02:41:13 - "Test Case 'Common tests' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 1 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Case 'Common tests' finished: PASSED_MANUAL"
12.12.2017 02:41:13 - "Test Case 'Hierarchy level' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 1 ms"
12.12.2017 02:41:13 - "Test Case 'Hierarchy level' finished: PASSED"
12.12.2017 02:41:13 - "Test Case 'Dataset (series) tests' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 1 ms"
12.12.2017 02:41:13 - "Checking URL: 'http://www.geoportal.sk'"
12.12.2017 02:41:13 - "Checking URL: 'https://www.geoportal.sk/'"
12.12.2017 02:41:13 - "Exception: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target URL: https://www.geoportal.sk/"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.3: Dataset linkage': FAILED - 182 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 1 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 1 ms"
12.12.2017 02:41:13 - "Test Case 'Dataset (series) tests' finished: FAILED"
12.12.2017 02:41:13 - "Test Case 'Service tests' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Case 'Service tests' finished: PASSED"
12.12.2017 02:41:13 - "Test Case 'Keywords' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Case 'Keywords' finished: PASSED"
12.12.2017 02:41:13 - "Test Case 'Keywords - details' started"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
12.12.2017 02:41:13 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 462 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Case 'Keywords - details' finished: PASSED"
12.12.2017 02:41:13 - "Test Case 'Temporal extent' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
12.12.2017 02:41:13 - "Test Case 'Temporal extent' finished: PASSED"
12.12.2017 02:41:13 - "Test Case 'Temporal extent - details' started"
12.12.2017 02:41:13 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 1 ms"
12.12.2017 02:41:13 - "Test Case 'Temporal extent - details' finished: PASSED"
12.12.2017 02:41:13 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
12.12.2017 02:41:14 - Releasing resources
12.12.2017 02:41:14 - Changed state from INITIALIZED to RUNNING
12.12.2017 02:41:14 - Duration: 18sec
12.12.2017 02:41:14 - TestRun finished
12.12.2017 02:41:14 - Changed state from RUNNING to COMPLETED
