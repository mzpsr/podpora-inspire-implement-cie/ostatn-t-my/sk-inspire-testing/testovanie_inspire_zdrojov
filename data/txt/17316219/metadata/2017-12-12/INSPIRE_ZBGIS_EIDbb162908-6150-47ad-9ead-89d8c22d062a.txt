12.12.2017 02:17:03 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_INSPIRE ZBGIS (initiated Tue Dec 12 02:17:03 CET 2017)
12.12.2017 02:17:03 - Resolving Executable Test Suite dependencies
12.12.2017 02:17:03 - Preparing 2 Test Task:
12.12.2017 02:17:03 -  TestTask 1 (85183002-8c4d-4823-930e-fc70361e30c5)
12.12.2017 02:17:03 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.12.2017 02:17:03 -  with parameters: 
12.12.2017 02:17:03 - etf.testcases = *
12.12.2017 02:17:03 -  TestTask 2 (b7f68b0d-99b9-423c-9c27-7a74fdd4b617)
12.12.2017 02:17:03 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
12.12.2017 02:17:03 -  with parameters: 
12.12.2017 02:17:03 - etf.testcases = *
12.12.2017 02:17:03 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 02:17:03 - Setting state to CREATED
12.12.2017 02:17:03 - Changed state from CREATED to INITIALIZING
12.12.2017 02:17:03 - Starting TestRun.bb162908-6150-47ad-9ead-89d8c22d062a at 2017-12-12T02:17:05+01:00
12.12.2017 02:17:05 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 02:17:05 - TestRunTask initialized
12.12.2017 02:17:05 - Creating new tests databases to speed up tests.
12.12.2017 02:17:05 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:17:05 - Optimizing last database etf-tdb-8fbf3b6b-e05a-498f-8c7d-bc8c3e729101-0 
12.12.2017 02:17:05 - Import completed
12.12.2017 02:17:05 - Validation ended with 0 error(s)
12.12.2017 02:17:05 - Compiling test script
12.12.2017 02:17:05 - Starting XQuery tests
12.12.2017 02:17:05 - "Testing 1 records"
12.12.2017 02:17:05 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.12.2017 02:17:05 - "Statistics table: 0 ms"
12.12.2017 02:17:05 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.12.2017 02:17:05 - "Test Case 'Schema validation' started"
12.12.2017 02:17:07 - "Validating file GetRecordByIdResponse.xml: 1962 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1963 ms"
12.12.2017 02:17:07 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 02:17:07 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
12.12.2017 02:17:07 - Releasing resources
12.12.2017 02:17:07 - TestRunTask initialized
12.12.2017 02:17:07 - Recreating new tests databases as the Test Object has changed!
12.12.2017 02:17:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:17:07 - Optimizing last database etf-tdb-8fbf3b6b-e05a-498f-8c7d-bc8c3e729101-0 
12.12.2017 02:17:07 - Import completed
12.12.2017 02:17:07 - Validation ended with 0 error(s)
12.12.2017 02:17:07 - Compiling test script
12.12.2017 02:17:07 - Starting XQuery tests
12.12.2017 02:17:07 - "Testing 1 records"
12.12.2017 02:17:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.12.2017 02:17:07 - "Statistics table: 1 ms"
12.12.2017 02:17:07 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.12.2017 02:17:07 - "Test Case 'Common tests' started"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 1 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Case 'Common tests' finished: PASSED_MANUAL"
12.12.2017 02:17:07 - "Test Case 'Hierarchy level' started"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Case 'Hierarchy level' finished: PASSED"
12.12.2017 02:17:07 - "Test Case 'Dataset (series) tests' started"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
12.12.2017 02:17:07 - "Test Case 'Dataset (series) tests' finished: PASSED"
12.12.2017 02:17:07 - "Test Case 'Service tests' started"
12.12.2017 02:17:07 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
12.12.2017 02:17:07 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get'"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 168 ms"
12.12.2017 02:17:08 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_..."
12.12.2017 02:17:08 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 315 ms"
12.12.2017 02:17:08 - "Test Case 'Service tests' finished: FAILED"
12.12.2017 02:17:08 - "Test Case 'Keywords' started"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
12.12.2017 02:17:08 - "Test Case 'Keywords' finished: PASSED"
12.12.2017 02:17:08 - "Test Case 'Keywords - details' started"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
12.12.2017 02:17:08 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 22 ms"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
12.12.2017 02:17:08 - "Test Case 'Keywords - details' finished: PASSED"
12.12.2017 02:17:08 - "Test Case 'Temporal extent' started"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
12.12.2017 02:17:08 - "Test Case 'Temporal extent' finished: PASSED"
12.12.2017 02:17:08 - "Test Case 'Temporal extent - details' started"
12.12.2017 02:17:08 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 1 ms"
12.12.2017 02:17:08 - "Test Case 'Temporal extent - details' finished: PASSED"
12.12.2017 02:17:08 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
12.12.2017 02:17:09 - Releasing resources
12.12.2017 02:17:09 - Changed state from INITIALIZED to RUNNING
12.12.2017 02:17:09 - Duration: 5sec
12.12.2017 02:17:09 - TestRun finished
12.12.2017 02:17:09 - Changed state from RUNNING to COMPLETED
