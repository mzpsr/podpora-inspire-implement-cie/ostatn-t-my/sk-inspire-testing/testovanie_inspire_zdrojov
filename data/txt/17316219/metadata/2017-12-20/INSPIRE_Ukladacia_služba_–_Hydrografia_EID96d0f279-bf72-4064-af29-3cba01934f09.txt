20.12.2017 00:08:24 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_INSPIRE Ukladacia služba &ndash; Hydrografia (initiated Wed Dec 20 00:08:24 CET 2017)
20.12.2017 00:08:24 - Resolving Executable Test Suite dependencies
20.12.2017 00:08:24 - Preparing 2 Test Task:
20.12.2017 00:08:24 -  TestTask 1 (0728574d-44ac-4ce4-ba2d-b8cfac5da4e6)
20.12.2017 00:08:24 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
20.12.2017 00:08:24 -  with parameters: 
20.12.2017 00:08:24 - etf.testcases = *
20.12.2017 00:08:24 -  TestTask 2 (73f69048-a803-469b-bdf5-6ec0600940cc)
20.12.2017 00:08:24 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
20.12.2017 00:08:24 -  with parameters: 
20.12.2017 00:08:24 - etf.testcases = *
20.12.2017 00:08:24 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
20.12.2017 00:08:24 - Setting state to CREATED
20.12.2017 00:08:24 - Changed state from CREATED to INITIALIZING
20.12.2017 00:08:24 - Starting TestRun.96d0f279-bf72-4064-af29-3cba01934f09 at 2017-12-20T00:08:26+01:00
20.12.2017 00:08:26 - Changed state from INITIALIZING to INITIALIZED
20.12.2017 00:08:26 - TestRunTask initialized
20.12.2017 00:08:26 - Creating new tests databases to speed up tests.
20.12.2017 00:08:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
20.12.2017 00:08:26 - Optimizing last database etf-tdb-59cce6ae-7548-434e-a971-3806a406dc76-0 
20.12.2017 00:08:26 - Import completed
20.12.2017 00:08:33 - Validation ended with 0 error(s)
20.12.2017 00:08:33 - Compiling test script
20.12.2017 00:08:33 - Starting XQuery tests
20.12.2017 00:08:33 - "Testing 1 records"
20.12.2017 00:08:33 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
20.12.2017 00:08:33 - "Statistics table: 1 ms"
20.12.2017 00:08:33 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
20.12.2017 00:08:33 - "Test Case 'Schema validation' started"
20.12.2017 00:08:34 - "Validating file GetRecordByIdResponse.xml: 1552 ms"
20.12.2017 00:08:34 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1552 ms"
20.12.2017 00:08:34 - "Test Case 'Schema validation' finished: PASSED"
20.12.2017 00:08:34 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
20.12.2017 00:08:35 - Releasing resources
20.12.2017 00:08:35 - TestRunTask initialized
20.12.2017 00:08:35 - Recreating new tests databases as the Test Object has changed!
20.12.2017 00:08:35 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
20.12.2017 00:08:35 - Optimizing last database etf-tdb-59cce6ae-7548-434e-a971-3806a406dc76-0 
20.12.2017 00:08:35 - Import completed
20.12.2017 00:08:35 - Validation ended with 0 error(s)
20.12.2017 00:08:35 - Compiling test script
20.12.2017 00:08:35 - Starting XQuery tests
20.12.2017 00:08:35 - "Testing 1 records"
20.12.2017 00:08:35 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
20.12.2017 00:08:35 - "Statistics table: 0 ms"
20.12.2017 00:08:35 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
20.12.2017 00:08:35 - "Test Case 'Common tests' started"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 1 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 1 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Case 'Common tests' finished: PASSED_MANUAL"
20.12.2017 00:08:35 - "Test Case 'Hierarchy level' started"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Case 'Hierarchy level' finished: PASSED"
20.12.2017 00:08:35 - "Test Case 'Dataset (series) tests' started"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
20.12.2017 00:08:35 - "Test Case 'Dataset (series) tests' finished: PASSED"
20.12.2017 00:08:35 - "Test Case 'Service tests' started"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
20.12.2017 00:08:35 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_hydrography_wfs/service.svc/get'"
20.12.2017 00:08:35 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 150 ms"
20.12.2017 00:08:35 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_..."
20.12.2017 00:09:00 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 24510 ms"
20.12.2017 00:09:00 - "Test Case 'Service tests' finished: FAILED"
20.12.2017 00:09:00 - "Test Case 'Keywords' started"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
20.12.2017 00:09:00 - "Test Case 'Keywords' finished: PASSED"
20.12.2017 00:09:00 - "Test Case 'Keywords - details' started"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
20.12.2017 00:09:00 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 35 ms"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 1 ms"
20.12.2017 00:09:00 - "Test Case 'Keywords - details' finished: PASSED"
20.12.2017 00:09:00 - "Test Case 'Temporal extent' started"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
20.12.2017 00:09:00 - "Test Case 'Temporal extent' finished: PASSED"
20.12.2017 00:09:00 - "Test Case 'Temporal extent - details' started"
20.12.2017 00:09:00 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
20.12.2017 00:09:00 - "Test Case 'Temporal extent - details' finished: PASSED"
20.12.2017 00:09:00 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
20.12.2017 00:09:01 - Releasing resources
20.12.2017 00:09:01 - Changed state from INITIALIZED to RUNNING
20.12.2017 00:09:01 - Duration: 36sec
20.12.2017 00:09:01 - TestRun finished
20.12.2017 00:09:01 - Changed state from RUNNING to COMPLETED
