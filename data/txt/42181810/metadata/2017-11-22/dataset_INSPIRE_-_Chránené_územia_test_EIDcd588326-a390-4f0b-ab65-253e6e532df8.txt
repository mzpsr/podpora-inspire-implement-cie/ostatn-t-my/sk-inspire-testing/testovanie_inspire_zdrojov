22.11.2017 00:00:16 - Preparing Test Run metadata (initiated Wed Nov 22 00:00:16 CET 2017)
22.11.2017 00:00:16 - Resolving Executable Test Suite dependencies
22.11.2017 00:00:16 - Preparing 2 Test Task:
22.11.2017 00:00:16 -  TestTask 1 (cffc679c-7f2f-490b-9547-5a8ced1c7fe3)
22.11.2017 00:00:16 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
22.11.2017 00:00:16 -  with parameters: 
22.11.2017 00:00:16 - etf.testcases = *
22.11.2017 00:00:16 -  TestTask 2 (04d567c7-2a34-41e4-ad62-dd0c119d97fc)
22.11.2017 00:00:16 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
22.11.2017 00:00:16 -  with parameters: 
22.11.2017 00:00:16 - etf.testcases = *
22.11.2017 00:00:16 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
22.11.2017 00:00:16 - Setting state to CREATED
22.11.2017 00:00:16 - Changed state from CREATED to INITIALIZING
22.11.2017 00:00:16 - Starting TestRun.cd588326-a390-4f0b-ab65-253e6e532df8 at 2017-11-22T00:00:18+01:00
22.11.2017 00:00:18 - Changed state from INITIALIZING to INITIALIZED
22.11.2017 00:00:18 - TestRunTask initialized
22.11.2017 00:00:18 - Creating new tests databases to speed up tests.
22.11.2017 00:00:18 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 00:00:18 - Optimizing last database etf-tdb-6b2fbdbd-a3fe-404c-88cc-4374bea9da2a-0 
22.11.2017 00:00:18 - Import completed
22.11.2017 00:00:18 - Validation ended with 0 error(s)
22.11.2017 00:00:18 - Compiling test script
22.11.2017 00:00:18 - Starting XQuery tests
22.11.2017 00:00:18 - "Testing 1 records"
22.11.2017 00:00:18 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
22.11.2017 00:00:18 - "Statistics table: 1 ms"
22.11.2017 00:00:18 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
22.11.2017 00:00:18 - "Test Case 'Schema validation' started"
22.11.2017 00:00:22 - "Validating file GetRecordByIdResponse.xml: 3198 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 3199 ms"
22.11.2017 00:00:22 - "Test Case 'Schema validation' finished: PASSED"
22.11.2017 00:00:22 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
22.11.2017 00:00:22 - Releasing resources
22.11.2017 00:00:22 - TestRunTask initialized
22.11.2017 00:00:22 - Recreating new tests databases as the Test Object has changed!
22.11.2017 00:00:22 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 00:00:22 - Optimizing last database etf-tdb-6b2fbdbd-a3fe-404c-88cc-4374bea9da2a-0 
22.11.2017 00:00:22 - Import completed
22.11.2017 00:00:22 - Validation ended with 0 error(s)
22.11.2017 00:00:22 - Compiling test script
22.11.2017 00:00:22 - Starting XQuery tests
22.11.2017 00:00:22 - "Testing 1 records"
22.11.2017 00:00:22 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
22.11.2017 00:00:22 - "Statistics table: 1 ms"
22.11.2017 00:00:22 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
22.11.2017 00:00:22 - "Test Case 'Common tests' started"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.1: Title': PASSED - 2 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 1 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Case 'Common tests' finished: PASSED_MANUAL"
22.11.2017 00:00:22 - "Test Case 'Hierarchy level' started"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Case 'Hierarchy level' finished: PASSED"
22.11.2017 00:00:22 - "Test Case 'Dataset (series) tests' started"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
22.11.2017 00:00:22 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
22.11.2017 00:00:22 - "Checking URL: 'http://www.google.com/url?q=http%3A%2F%2Finspire.biomonitoring.sk%2Fgeoserver%2Fows%3Fservice%3DWFS%26count%3D1%26REQUEST%3DGetFeature%26version%3D2.0.0%26namespaces%3Dxmlns%28ps-f%2Curn%3Ax-inspire%3Aspecification%3Agmlas%3AProtectedSitesFull%3A3.0%29%26TYPENAMES%3Dps-f%3AProtectedSite&amp;sa=D&amp;sntz=1&amp;usg=AFQjCNGIfcsBb9W0syRLy4A3ll6H9Uenkw'"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED_MANUAL - 124 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 1 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 1 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Case 'Dataset (series) tests' finished: PASSED_MANUAL"
22.11.2017 00:00:23 - "Test Case 'Service tests' started"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 1 ms"
22.11.2017 00:00:23 - "Test Case 'Service tests' finished: PASSED"
22.11.2017 00:00:23 - "Test Case 'Keywords' started"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Case 'Keywords' finished: PASSED"
22.11.2017 00:00:23 - "Test Case 'Keywords - details' started"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
22.11.2017 00:00:23 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 542 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 1 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Case 'Keywords - details' finished: PASSED"
22.11.2017 00:00:23 - "Test Case 'Temporal extent' started"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Case 'Temporal extent' finished: PASSED"
22.11.2017 00:00:23 - "Test Case 'Temporal extent - details' started"
22.11.2017 00:00:23 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
22.11.2017 00:00:23 - "Test Case 'Temporal extent - details' finished: PASSED"
22.11.2017 00:00:23 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
22.11.2017 00:00:24 - Releasing resources
22.11.2017 00:00:24 - Changed state from INITIALIZED to RUNNING
22.11.2017 00:00:24 - Duration: 7sec
22.11.2017 00:00:24 - TestRun finished
22.11.2017 00:00:24 - Changed state from RUNNING to COMPLETED
