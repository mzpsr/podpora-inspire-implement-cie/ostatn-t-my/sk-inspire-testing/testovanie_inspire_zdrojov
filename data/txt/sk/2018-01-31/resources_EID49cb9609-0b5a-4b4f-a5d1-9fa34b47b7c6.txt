31.01.2018 00:53:54 - Preparing Test Run sk_testing_inspire_resources_app__undefined__RUN__EIDed2d3501-d700-4ff9-b9bf-070dece8ddbd__2 (initiated Wed Jan 31 00:53:54 CET 2018)
31.01.2018 00:53:54 - Resolving Executable Test Suite dependencies
31.01.2018 00:53:54 - Preparing 2 Test Task:
31.01.2018 00:53:54 -  TestTask 1 (c5cf3ce8-09e2-4a73-bba1-bdce4a3d6aca)
31.01.2018 00:53:54 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'WFS 2.0 (OGC 09-025r2/ISO 19142) Conformance Test Suite (EID: 95a1b6fc-2b55-3d43-9502-3b8b605bda10, V: 1.26.0 )'
31.01.2018 00:53:54 -  with parameters: 
31.01.2018 00:53:54 - etf.testcases = *
31.01.2018 00:53:54 -  TestTask 2 (217c0412-1941-4caa-a640-a18cb6a3e25e)
31.01.2018 00:53:54 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'Conformance Class: Download Service - Direct WFS (EID: ed2d3501-d700-4ff9-b9bf-070dece8ddbd, V: 1.0.2 )'
31.01.2018 00:53:54 -  with parameters: 
31.01.2018 00:53:54 - etf.testcases = *
31.01.2018 00:53:54 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 00:53:54 - Setting state to CREATED
31.01.2018 00:53:54 - Changed state from CREATED to INITIALIZING
31.01.2018 00:53:54 - Starting TestRun.49cb9609-0b5a-4b4f-a5d1-9fa34b47b7c6 at 2018-01-31T00:53:55+01:00
31.01.2018 00:53:55 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 00:53:55 - TestRunTask initialized
31.01.2018 00:53:55 - Invoking TEAM Engine remotely. This may take a while. Progress messages are not supported.
31.01.2018 00:53:55 - Timeout is set to: 20min
31.01.2018 00:56:47 - Results received.
31.01.2018 00:56:48 - Internal ETS model updated.
31.01.2018 00:56:48 - Transforming results.
31.01.2018 00:56:48 - 49 of 4969 assertions passed
31.01.2018 00:56:49 - Releasing resources
31.01.2018 00:56:49 - Project Properties: 
31.01.2018 00:56:49 - etf.testcases - * 
31.01.2018 00:56:49 - serviceEndpoint - https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/uo_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service?ACCEPTVERSIONS=2.0.0&request=GetCapabilities&service=WFS&VERSION=2.0.0 
31.01.2018 00:56:49 - username -  
31.01.2018 00:56:49 - authUser -  
31.01.2018 00:56:49 - authMethod - basic 
31.01.2018 00:56:49 - TestRunTask initialized
31.01.2018 00:56:56 - Releasing resources
31.01.2018 00:56:56 - Changed state from INITIALIZED to RUNNING
31.01.2018 00:56:56 - Duration: 3min
31.01.2018 00:56:56 - TestRun finished
31.01.2018 00:56:56 - Changed state from RUNNING to COMPLETED
