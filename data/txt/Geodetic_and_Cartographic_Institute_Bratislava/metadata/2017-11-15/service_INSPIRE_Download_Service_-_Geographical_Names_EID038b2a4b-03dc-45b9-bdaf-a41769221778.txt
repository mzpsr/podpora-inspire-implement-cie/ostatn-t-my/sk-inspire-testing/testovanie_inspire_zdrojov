15.11.2017 21:09:44 - Preparing Test Run metadata (initiated Wed Nov 15 21:09:44 CET 2017)
15.11.2017 21:09:44 - Resolving Executable Test Suite dependencies
15.11.2017 21:09:44 - Preparing 2 Test Task:
15.11.2017 21:09:44 -  TestTask 1 (b205dc85-1fa7-4031-88bf-aecbd886bdde)
15.11.2017 21:09:44 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
15.11.2017 21:09:44 -  with parameters: 
15.11.2017 21:09:44 - etf.testcases = *
15.11.2017 21:09:44 -  TestTask 2 (05d3cb13-3804-4280-8d4a-39a163fa83c4)
15.11.2017 21:09:44 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
15.11.2017 21:09:44 -  with parameters: 
15.11.2017 21:09:44 - etf.testcases = *
15.11.2017 21:09:44 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
15.11.2017 21:09:44 - Setting state to CREATED
15.11.2017 21:09:44 - Changed state from CREATED to INITIALIZING
15.11.2017 21:09:44 - Starting TestRun.038b2a4b-03dc-45b9-bdaf-a41769221778 at 2017-11-15T21:09:46+01:00
15.11.2017 21:09:46 - Changed state from INITIALIZING to INITIALIZED
15.11.2017 21:09:46 - TestRunTask initialized
15.11.2017 21:09:46 - Creating new tests databases to speed up tests.
15.11.2017 21:09:46 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 21:09:46 - Optimizing last database etf-tdb-fb181125-7f51-4b08-9834-15bce3173767-0 
15.11.2017 21:09:46 - Import completed
15.11.2017 21:09:46 - Validation ended with 0 error(s)
15.11.2017 21:09:46 - Compiling test script
15.11.2017 21:09:46 - Starting XQuery tests
15.11.2017 21:09:46 - "Testing 1 records"
15.11.2017 21:09:46 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
15.11.2017 21:09:46 - "Statistics table: 1 ms"
15.11.2017 21:09:46 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
15.11.2017 21:09:46 - "Test Case 'Schema validation' started"
15.11.2017 21:09:48 - "Validating file GetRecordByIdResponse.xml: 1929 ms"
15.11.2017 21:09:48 - "Test Assertion 'md-xml.a.1: Validate XML documents': FAILED - 1930 ms"
15.11.2017 21:09:48 - "Test Case 'Schema validation' finished: FAILED"
15.11.2017 21:09:48 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: FAILED"
15.11.2017 21:09:53 - Releasing resources
15.11.2017 21:09:53 - TestRunTask initialized
15.11.2017 21:09:53 - Recreating new tests databases as the Test Object has changed!
15.11.2017 21:09:53 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 21:09:53 - Optimizing last database etf-tdb-fb181125-7f51-4b08-9834-15bce3173767-0 
15.11.2017 21:09:53 - Import completed
15.11.2017 21:09:54 - Validation ended with 0 error(s)
15.11.2017 21:09:54 - Compiling test script
15.11.2017 21:09:54 - Starting XQuery tests
15.11.2017 21:09:54 - "Testing 1 records"
15.11.2017 21:09:54 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
15.11.2017 21:09:54 - "Statistics table: 0 ms"
15.11.2017 21:09:54 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
15.11.2017 21:09:54 - "Test Case 'Common tests' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.3: Access and use conditions': FAILED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.4: Public access': FAILED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.6: Language': FAILED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.10: Responsible party contact info': FAILED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.a.11: Responsible party role': FAILED - 0 ms"
15.11.2017 21:09:54 - "Test Case 'Common tests' finished: FAILED"
15.11.2017 21:09:54 - "Test Case 'Hierarchy level' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Case 'Hierarchy level' finished: PASSED"
15.11.2017 21:09:54 - "Test Case 'Dataset (series) tests' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Case 'Dataset (series) tests' finished: PASSED"
15.11.2017 21:09:54 - "Test Case 'Service tests' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.d.1: Service type': FAILED - 0 ms"
15.11.2017 21:09:54 - "Checking URL: 'https://test-zbgisws.skgeodesy.sk/inspire_geographical_names_wfs/service.svc/get'"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 181 ms"
15.11.2017 21:09:54 - "Checking URL: 'https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:a4bb447e-966e-4be1-a6f9-b262ca12218d-gn:NamedPlace'"
15.11.2017 21:09:54 - "Exception: handshake alert:  unrecognized_name URL: https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:a4bb447e-966e-4be1-a6f9-b262ca12218d-gn:NamedPlace"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 55 ms"
15.11.2017 21:09:54 - "Test Case 'Service tests' finished: FAILED"
15.11.2017 21:09:54 - "Test Case 'Keywords' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Case 'Keywords' finished: PASSED"
15.11.2017 21:09:54 - "Test Case 'Keywords - details' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
15.11.2017 21:09:54 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 8 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Case 'Keywords - details' finished: PASSED"
15.11.2017 21:09:54 - "Test Case 'Temporal extent' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.g.1: Temporal extent': FAILED - 1 ms"
15.11.2017 21:09:54 - "Test Case 'Temporal extent' finished: FAILED"
15.11.2017 21:09:54 - "Test Case 'Temporal extent - details' started"
15.11.2017 21:09:54 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
15.11.2017 21:09:54 - "Test Case 'Temporal extent - details' finished: PASSED"
15.11.2017 21:09:54 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
15.11.2017 21:09:55 - Releasing resources
15.11.2017 21:09:55 - Changed state from INITIALIZED to RUNNING
15.11.2017 21:09:55 - Duration: 11sec
15.11.2017 21:09:55 - TestRun finished
15.11.2017 21:09:55 - Changed state from RUNNING to COMPLETED
