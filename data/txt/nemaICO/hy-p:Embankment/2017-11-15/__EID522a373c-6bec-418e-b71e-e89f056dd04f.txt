15.11.2017 23:58:31 - Preparing Test Run hy-p:Embankment (initiated Wed Nov 15 23:58:31 CET 2017)
15.11.2017 23:58:31 - Resolving Executable Test Suite dependencies
15.11.2017 23:58:31 - Preparing 10 Test Task:
15.11.2017 23:58:31 -  TestTask 1 (0f9687f8-c075-4f16-a275-1add828d1d27)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 2 (f34b8577-318b-45bd-8529-82091deff139)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.3 )'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 3 (601142a4-4f5c-4f7c-be96-c54956eab38f)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 4 (4326762d-059f-4b84-a22c-f5e3760d3564)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 5 (000158ac-8350-4f18-ba64-fd8dbc0bd9bd)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 6 (33ea2c04-ad4c-4548-af11-a2cec9a96590)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 7 (29e82759-0bf8-4770-a10c-98764fd18996)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 8 (616438e2-2013-4e01-9af2-970fc2698885)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 9 (cb54ce1a-fdfb-412b-902a-14fab392defd)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 -  TestTask 10 (0aa00db9-5c32-487e-9de6-aabcc285d137)
15.11.2017 23:58:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
15.11.2017 23:58:31 -  with parameters: 
15.11.2017 23:58:31 - etf.testcases = *
15.11.2017 23:58:31 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
15.11.2017 23:58:31 - Setting state to CREATED
15.11.2017 23:58:31 - Changed state from CREATED to INITIALIZING
15.11.2017 23:58:31 - Starting TestRun.522a373c-6bec-418e-b71e-e89f056dd04f at 2017-11-15T23:58:32+01:00
15.11.2017 23:58:32 - Changed state from INITIALIZING to INITIALIZED
15.11.2017 23:58:32 - TestRunTask initialized
15.11.2017 23:58:32 - Creating new tests databases to speed up tests.
15.11.2017 23:58:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:32 - Optimizing last database etf-tdb-3484e893-808f-442d-a03b-7f098ed8cc2a-0 
15.11.2017 23:58:32 - Import completed
15.11.2017 23:58:33 - Validation ended with 0 error(s)
15.11.2017 23:58:33 - Compiling test script
15.11.2017 23:58:33 - Starting XQuery tests
15.11.2017 23:58:33 - "Testing 1 features"
15.11.2017 23:58:33 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
15.11.2017 23:58:33 - "Statistics table: 1 ms"
15.11.2017 23:58:33 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
15.11.2017 23:58:33 - "Test Case 'Basic tests' started"
15.11.2017 23:58:33 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
15.11.2017 23:58:33 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
15.11.2017 23:58:33 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
15.11.2017 23:58:33 - "Test Case 'Basic tests' finished: PASSED"
15.11.2017 23:58:33 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
15.11.2017 23:58:36 - Releasing resources
15.11.2017 23:58:36 - TestRunTask initialized
15.11.2017 23:58:36 - Recreating new tests databases as the Test Object has changed!
15.11.2017 23:58:36 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:36 - Optimizing last database etf-tdb-3484e893-808f-442d-a03b-7f098ed8cc2a-0 
15.11.2017 23:58:36 - Import completed
15.11.2017 23:58:36 - Validation ended with 0 error(s)
15.11.2017 23:58:36 - Compiling test script
15.11.2017 23:58:36 - Starting XQuery tests
15.11.2017 23:58:36 - "Testing 1 features"
15.11.2017 23:58:36 - "Indexing features (parsing errors: 0): 159 ms"
15.11.2017 23:58:36 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
15.11.2017 23:58:36 - "Statistics table: 0 ms"
15.11.2017 23:58:36 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
15.11.2017 23:58:36 - "Test Case 'Schema' started"
15.11.2017 23:58:36 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
15.11.2017 23:58:36 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
15.11.2017 23:58:36 - "Test Case 'Schema' finished: PASSED_MANUAL"
15.11.2017 23:58:36 - "Test Case 'Schema validation' started"
15.11.2017 23:58:36 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
15.11.2017 23:58:36 - "Validating get.xml"
15.11.2017 23:58:47 - "Duration: 10319 ms. Errors: 0."
15.11.2017 23:58:47 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 10320 ms"
15.11.2017 23:58:47 - "Test Case 'Schema validation' finished: PASSED"
15.11.2017 23:58:47 - "Test Case 'GML model' started"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 1 ms"
15.11.2017 23:58:47 - "Test Case 'GML model' finished: PASSED"
15.11.2017 23:58:47 - "Test Case 'Simple features' started"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 200 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Case 'Simple features' finished: PASSED"
15.11.2017 23:58:47 - "Test Case 'Code list values in basic data types' started"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 15 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 5 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 5 ms"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
15.11.2017 23:58:47 - "Test Case 'Code list values in basic data types' finished: PASSED"
15.11.2017 23:58:47 - "Test Case 'Constraints' started"
15.11.2017 23:58:47 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Case 'Constraints' finished: PASSED"
15.11.2017 23:58:47 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
15.11.2017 23:58:47 - Releasing resources
15.11.2017 23:58:47 - TestRunTask initialized
15.11.2017 23:58:47 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:47 - Validation ended with 0 error(s)
15.11.2017 23:58:47 - Compiling test script
15.11.2017 23:58:47 - Starting XQuery tests
15.11.2017 23:58:47 - "Testing 1 features"
15.11.2017 23:58:47 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
15.11.2017 23:58:47 - "Statistics table: 0 ms"
15.11.2017 23:58:47 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
15.11.2017 23:58:47 - "Test Case 'Basic test' started"
15.11.2017 23:58:47 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': PASSED - 0 ms"
15.11.2017 23:58:47 - "Test Case 'Basic test' finished: PASSED"
15.11.2017 23:58:47 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: PASSED"
15.11.2017 23:58:48 - Releasing resources
15.11.2017 23:58:48 - TestRunTask initialized
15.11.2017 23:58:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:48 - Validation ended with 0 error(s)
15.11.2017 23:58:48 - Compiling test script
15.11.2017 23:58:48 - Starting XQuery tests
15.11.2017 23:58:48 - "Testing 1 features"
15.11.2017 23:58:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
15.11.2017 23:58:48 - "Statistics table: 0 ms"
15.11.2017 23:58:48 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
15.11.2017 23:58:48 - "Test Case 'Code list values' started"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 11 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.a.2: type attributes': PASSED - 20 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 27 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 27 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 26 ms"
15.11.2017 23:58:48 - "Test Case 'Code list values' finished: PASSED"
15.11.2017 23:58:48 - "Test Case 'Geometry' started"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
15.11.2017 23:58:48 - "Test Case 'Geometry' finished: PASSED_MANUAL"
15.11.2017 23:58:48 - "Test Case 'Identifiers and references' started"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
15.11.2017 23:58:48 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
15.11.2017 23:58:48 - "Test Case 'Constraints' started"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
15.11.2017 23:58:48 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
15.11.2017 23:58:48 - "Test Case 'Constraints' finished: PASSED_MANUAL"
15.11.2017 23:58:48 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: PASSED_MANUAL"
15.11.2017 23:58:49 - Releasing resources
15.11.2017 23:58:49 - TestRunTask initialized
15.11.2017 23:58:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:49 - Validation ended with 0 error(s)
15.11.2017 23:58:49 - Compiling test script
15.11.2017 23:58:49 - Starting XQuery tests
15.11.2017 23:58:49 - "Testing 1 features"
15.11.2017 23:58:49 - "Indexing features (parsing errors: 0): 154 ms"
15.11.2017 23:58:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
15.11.2017 23:58:49 - "Statistics table: 1 ms"
15.11.2017 23:58:49 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
15.11.2017 23:58:49 - "Test Case 'Version consistency' started"
15.11.2017 23:58:49 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
15.11.2017 23:58:49 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
15.11.2017 23:58:49 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
15.11.2017 23:58:49 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
15.11.2017 23:58:49 - "Test Case 'Temporal consistency' started"
15.11.2017 23:58:49 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
15.11.2017 23:58:49 - "Test Case 'Temporal consistency' finished: PASSED"
15.11.2017 23:58:49 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
15.11.2017 23:58:49 - Releasing resources
15.11.2017 23:58:49 - TestRunTask initialized
15.11.2017 23:58:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:49 - Validation ended with 0 error(s)
15.11.2017 23:58:49 - Compiling test script
15.11.2017 23:58:49 - Starting XQuery tests
15.11.2017 23:58:49 - "Testing 1 features"
15.11.2017 23:58:50 - "Indexing features (parsing errors: 0): 216 ms"
15.11.2017 23:58:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
15.11.2017 23:58:50 - "Statistics table: 0 ms"
15.11.2017 23:58:50 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
15.11.2017 23:58:50 - "Test Case 'Spatial consistency' started"
15.11.2017 23:58:50 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 1 ms"
15.11.2017 23:58:50 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Case 'Thematic consistency' started"
15.11.2017 23:58:50 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Case 'Identifiers' started"
15.11.2017 23:58:50 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
15.11.2017 23:58:50 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
15.11.2017 23:58:50 - Releasing resources
15.11.2017 23:58:50 - TestRunTask initialized
15.11.2017 23:58:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:50 - Validation ended with 0 error(s)
15.11.2017 23:58:50 - Compiling test script
15.11.2017 23:58:50 - Starting XQuery tests
15.11.2017 23:58:50 - "Testing 1 features"
15.11.2017 23:58:50 - "Indexing features (parsing errors: 0): 153 ms"
15.11.2017 23:58:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
15.11.2017 23:58:50 - "Statistics table: 0 ms"
15.11.2017 23:58:50 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
15.11.2017 23:58:50 - "Test Case 'Coordinate reference systems (CRS)' started"
15.11.2017 23:58:50 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
15.11.2017 23:58:50 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
15.11.2017 23:58:50 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
15.11.2017 23:58:51 - Releasing resources
15.11.2017 23:58:51 - TestRunTask initialized
15.11.2017 23:58:51 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:51 - Validation ended with 0 error(s)
15.11.2017 23:58:51 - Compiling test script
15.11.2017 23:58:51 - Starting XQuery tests
15.11.2017 23:58:51 - "Testing 1 features"
15.11.2017 23:58:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
15.11.2017 23:58:51 - "Statistics table: 0 ms"
15.11.2017 23:58:51 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
15.11.2017 23:58:51 - "Test Case 'Code lists' started"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Case 'Code lists' finished: PASSED"
15.11.2017 23:58:51 - "Test Case 'Feature references' started"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
15.11.2017 23:58:51 - "Test Case 'Feature references' finished: PASSED"
15.11.2017 23:58:51 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
15.11.2017 23:58:52 - Releasing resources
15.11.2017 23:58:52 - TestRunTask initialized
15.11.2017 23:58:52 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:52 - Validation ended with 0 error(s)
15.11.2017 23:58:52 - Compiling test script
15.11.2017 23:58:52 - Starting XQuery tests
15.11.2017 23:58:52 - "Testing 1 features"
15.11.2017 23:58:52 - "Indexing features (parsing errors: 0): 159 ms"
15.11.2017 23:58:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
15.11.2017 23:58:52 - "Statistics table: 1 ms"
15.11.2017 23:58:52 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
15.11.2017 23:58:52 - "Test Case 'Spatial reference systems' started"
15.11.2017 23:58:52 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
15.11.2017 23:58:52 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
15.11.2017 23:58:52 - "Test Case 'Spatial reference systems' finished: FAILED"
15.11.2017 23:58:52 - "Test Case 'Temporal reference systems' started"
15.11.2017 23:58:52 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
15.11.2017 23:58:52 - "Test Case 'Temporal reference systems' finished: PASSED"
15.11.2017 23:58:52 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
15.11.2017 23:58:53 - Releasing resources
15.11.2017 23:58:53 - TestRunTask initialized
15.11.2017 23:58:53 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 23:58:53 - Validation ended with 0 error(s)
15.11.2017 23:58:53 - Compiling test script
15.11.2017 23:58:53 - Starting XQuery tests
15.11.2017 23:58:53 - "Testing 1 features"
15.11.2017 23:58:53 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
15.11.2017 23:58:53 - "Statistics table: 0 ms"
15.11.2017 23:58:53 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
15.11.2017 23:58:53 - "Test Case 'Units of measure' started"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
15.11.2017 23:58:53 - "Test Case 'Units of measure' finished: PASSED"
15.11.2017 23:58:53 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
15.11.2017 23:58:53 - Releasing resources
15.11.2017 23:58:53 - Changed state from INITIALIZED to RUNNING
15.11.2017 23:58:54 - Duration: 22sec
15.11.2017 23:58:54 - TestRun finished
15.11.2017 23:58:54 - Changed state from RUNNING to COMPLETED
