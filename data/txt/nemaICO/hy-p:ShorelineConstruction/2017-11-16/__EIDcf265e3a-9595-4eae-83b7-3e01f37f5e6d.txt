16.11.2017 00:37:13 - Preparing Test Run hy-p:ShorelineConstruction (initiated Thu Nov 16 00:37:13 CET 2017)
16.11.2017 00:37:13 - Resolving Executable Test Suite dependencies
16.11.2017 00:37:13 - Preparing 10 Test Task:
16.11.2017 00:37:13 -  TestTask 1 (4dd02459-fd5e-4c90-b878-9850d3ed8b9d)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 2 (2e0cbdae-c3d7-467d-9de3-696dc530e7b3)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.3 )'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 3 (90bdbe7a-266d-4d11-bf72-f5e6d6e33f64)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 4 (c45c2579-8d6b-4119-8734-41f57d9f747c)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 5 (81423d40-fa74-4729-8e8e-54b59d932197)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 6 (e871a2af-9593-4335-889e-e4f257758b3f)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 7 (1eb44026-ae78-40f1-983e-294cc3f0c190)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 8 (4afa31b7-442b-4ffe-9fb1-1334d030600d)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 9 (ffff05b5-74ae-4034-a68f-7944e8c0f0e7)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 -  TestTask 10 (a1093417-418b-46f5-a582-391cf074cffa)
16.11.2017 00:37:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
16.11.2017 00:37:13 -  with parameters: 
16.11.2017 00:37:13 - etf.testcases = *
16.11.2017 00:37:13 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
16.11.2017 00:37:13 - Setting state to CREATED
16.11.2017 00:37:13 - Changed state from CREATED to INITIALIZING
16.11.2017 00:37:13 - Starting TestRun.cf265e3a-9595-4eae-83b7-3e01f37f5e6d at 2017-11-16T00:37:15+01:00
16.11.2017 00:37:15 - Changed state from INITIALIZING to INITIALIZED
16.11.2017 00:37:15 - TestRunTask initialized
16.11.2017 00:37:15 - Creating new tests databases to speed up tests.
16.11.2017 00:37:15 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:15 - Optimizing last database etf-tdb-929a80b2-4dc9-449f-b836-c64579ec9a14-0 
16.11.2017 00:37:15 - Import completed
16.11.2017 00:37:16 - Validation ended with 0 error(s)
16.11.2017 00:37:16 - Compiling test script
16.11.2017 00:37:16 - Starting XQuery tests
16.11.2017 00:37:16 - "Testing 1 features"
16.11.2017 00:37:16 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
16.11.2017 00:37:16 - "Statistics table: 1 ms"
16.11.2017 00:37:16 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
16.11.2017 00:37:16 - "Test Case 'Basic tests' started"
16.11.2017 00:37:16 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
16.11.2017 00:37:16 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
16.11.2017 00:37:16 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
16.11.2017 00:37:16 - "Test Case 'Basic tests' finished: PASSED"
16.11.2017 00:37:16 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
16.11.2017 00:37:20 - Releasing resources
16.11.2017 00:37:20 - TestRunTask initialized
16.11.2017 00:37:20 - Recreating new tests databases as the Test Object has changed!
16.11.2017 00:37:20 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:20 - Optimizing last database etf-tdb-929a80b2-4dc9-449f-b836-c64579ec9a14-0 
16.11.2017 00:37:20 - Import completed
16.11.2017 00:37:20 - Validation ended with 0 error(s)
16.11.2017 00:37:20 - Compiling test script
16.11.2017 00:37:20 - Starting XQuery tests
16.11.2017 00:37:20 - "Testing 1 features"
16.11.2017 00:37:20 - "Indexing features (parsing errors: 0): 149 ms"
16.11.2017 00:37:20 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
16.11.2017 00:37:20 - "Statistics table: 0 ms"
16.11.2017 00:37:20 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
16.11.2017 00:37:20 - "Test Case 'Schema' started"
16.11.2017 00:37:20 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
16.11.2017 00:37:20 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
16.11.2017 00:37:20 - "Test Case 'Schema' finished: PASSED_MANUAL"
16.11.2017 00:37:20 - "Test Case 'Schema validation' started"
16.11.2017 00:37:20 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
16.11.2017 00:37:20 - "Validating get.xml"
16.11.2017 00:37:33 - "Duration: 12315 ms. Errors: 0."
16.11.2017 00:37:33 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 12316 ms"
16.11.2017 00:37:33 - "Test Case 'Schema validation' finished: PASSED"
16.11.2017 00:37:33 - "Test Case 'GML model' started"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 1 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Case 'GML model' finished: PASSED"
16.11.2017 00:37:33 - "Test Case 'Simple features' started"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 190 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Case 'Simple features' finished: PASSED"
16.11.2017 00:37:33 - "Test Case 'Code list values in basic data types' started"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 170 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 5 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 7 ms"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
16.11.2017 00:37:33 - "Test Case 'Code list values in basic data types' finished: PASSED"
16.11.2017 00:37:33 - "Test Case 'Constraints' started"
16.11.2017 00:37:33 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
16.11.2017 00:37:33 - "Test Case 'Constraints' finished: PASSED"
16.11.2017 00:37:33 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
16.11.2017 00:37:35 - Releasing resources
16.11.2017 00:37:35 - TestRunTask initialized
16.11.2017 00:37:35 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:35 - Validation ended with 0 error(s)
16.11.2017 00:37:35 - Compiling test script
16.11.2017 00:37:35 - Starting XQuery tests
16.11.2017 00:37:35 - "Testing 1 features"
16.11.2017 00:37:35 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
16.11.2017 00:37:35 - "Statistics table: 1 ms"
16.11.2017 00:37:35 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
16.11.2017 00:37:35 - "Test Case 'Basic test' started"
16.11.2017 00:37:35 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': PASSED - 0 ms"
16.11.2017 00:37:35 - "Test Case 'Basic test' finished: PASSED"
16.11.2017 00:37:35 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: PASSED"
16.11.2017 00:37:35 - Releasing resources
16.11.2017 00:37:35 - TestRunTask initialized
16.11.2017 00:37:35 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:35 - Validation ended with 0 error(s)
16.11.2017 00:37:35 - Compiling test script
16.11.2017 00:37:35 - Starting XQuery tests
16.11.2017 00:37:35 - "Testing 1 features"
16.11.2017 00:37:35 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
16.11.2017 00:37:35 - "Statistics table: 1 ms"
16.11.2017 00:37:35 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
16.11.2017 00:37:35 - "Test Case 'Code list values' started"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 5 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.a.2: type attributes': PASSED - 58 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 63 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 9 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 20 ms"
16.11.2017 00:37:35 - "Test Case 'Code list values' finished: PASSED"
16.11.2017 00:37:35 - "Test Case 'Geometry' started"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
16.11.2017 00:37:35 - "Test Case 'Geometry' finished: PASSED_MANUAL"
16.11.2017 00:37:35 - "Test Case 'Identifiers and references' started"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
16.11.2017 00:37:35 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
16.11.2017 00:37:35 - "Test Case 'Constraints' started"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
16.11.2017 00:37:35 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
16.11.2017 00:37:35 - "Test Case 'Constraints' finished: PASSED_MANUAL"
16.11.2017 00:37:35 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: PASSED_MANUAL"
16.11.2017 00:37:36 - Releasing resources
16.11.2017 00:37:36 - TestRunTask initialized
16.11.2017 00:37:36 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:36 - Validation ended with 0 error(s)
16.11.2017 00:37:36 - Compiling test script
16.11.2017 00:37:36 - Starting XQuery tests
16.11.2017 00:37:36 - "Testing 1 features"
16.11.2017 00:37:36 - "Indexing features (parsing errors: 0): 149 ms"
16.11.2017 00:37:36 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
16.11.2017 00:37:36 - "Statistics table: 0 ms"
16.11.2017 00:37:36 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
16.11.2017 00:37:36 - "Test Case 'Version consistency' started"
16.11.2017 00:37:36 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
16.11.2017 00:37:36 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
16.11.2017 00:37:36 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
16.11.2017 00:37:36 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
16.11.2017 00:37:36 - "Test Case 'Temporal consistency' started"
16.11.2017 00:37:36 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
16.11.2017 00:37:36 - "Test Case 'Temporal consistency' finished: PASSED"
16.11.2017 00:37:36 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
16.11.2017 00:37:37 - Releasing resources
16.11.2017 00:37:37 - TestRunTask initialized
16.11.2017 00:37:37 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:37 - Validation ended with 0 error(s)
16.11.2017 00:37:37 - Compiling test script
16.11.2017 00:37:37 - Starting XQuery tests
16.11.2017 00:37:37 - "Testing 1 features"
16.11.2017 00:37:37 - "Indexing features (parsing errors: 0): 177 ms"
16.11.2017 00:37:37 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
16.11.2017 00:37:37 - "Statistics table: 1 ms"
16.11.2017 00:37:37 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
16.11.2017 00:37:37 - "Test Case 'Spatial consistency' started"
16.11.2017 00:37:37 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 0 ms"
16.11.2017 00:37:37 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Case 'Thematic consistency' started"
16.11.2017 00:37:37 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Case 'Identifiers' started"
16.11.2017 00:37:37 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
16.11.2017 00:37:37 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
16.11.2017 00:37:38 - Releasing resources
16.11.2017 00:37:38 - TestRunTask initialized
16.11.2017 00:37:38 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:38 - Validation ended with 0 error(s)
16.11.2017 00:37:38 - Compiling test script
16.11.2017 00:37:38 - Starting XQuery tests
16.11.2017 00:37:38 - "Testing 1 features"
16.11.2017 00:37:38 - "Indexing features (parsing errors: 0): 150 ms"
16.11.2017 00:37:38 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
16.11.2017 00:37:38 - "Statistics table: 0 ms"
16.11.2017 00:37:38 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
16.11.2017 00:37:38 - "Test Case 'Coordinate reference systems (CRS)' started"
16.11.2017 00:37:38 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 0 ms"
16.11.2017 00:37:38 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
16.11.2017 00:37:38 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
16.11.2017 00:37:40 - Releasing resources
16.11.2017 00:37:40 - TestRunTask initialized
16.11.2017 00:37:40 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:40 - Validation ended with 0 error(s)
16.11.2017 00:37:40 - Compiling test script
16.11.2017 00:37:40 - Starting XQuery tests
16.11.2017 00:37:40 - "Testing 1 features"
16.11.2017 00:37:40 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
16.11.2017 00:37:40 - "Statistics table: 0 ms"
16.11.2017 00:37:40 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
16.11.2017 00:37:40 - "Test Case 'Code lists' started"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Case 'Code lists' finished: PASSED"
16.11.2017 00:37:40 - "Test Case 'Feature references' started"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
16.11.2017 00:37:40 - "Test Case 'Feature references' finished: PASSED"
16.11.2017 00:37:40 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
16.11.2017 00:37:40 - Releasing resources
16.11.2017 00:37:40 - TestRunTask initialized
16.11.2017 00:37:40 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:40 - Validation ended with 0 error(s)
16.11.2017 00:37:40 - Compiling test script
16.11.2017 00:37:40 - Starting XQuery tests
16.11.2017 00:37:40 - "Testing 1 features"
16.11.2017 00:37:41 - "Indexing features (parsing errors: 0): 152 ms"
16.11.2017 00:37:41 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
16.11.2017 00:37:41 - "Statistics table: 0 ms"
16.11.2017 00:37:41 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
16.11.2017 00:37:41 - "Test Case 'Spatial reference systems' started"
16.11.2017 00:37:41 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
16.11.2017 00:37:41 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
16.11.2017 00:37:41 - "Test Case 'Spatial reference systems' finished: FAILED"
16.11.2017 00:37:41 - "Test Case 'Temporal reference systems' started"
16.11.2017 00:37:41 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
16.11.2017 00:37:41 - "Test Case 'Temporal reference systems' finished: PASSED"
16.11.2017 00:37:41 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
16.11.2017 00:37:42 - Releasing resources
16.11.2017 00:37:42 - TestRunTask initialized
16.11.2017 00:37:42 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:37:42 - Validation ended with 0 error(s)
16.11.2017 00:37:42 - Compiling test script
16.11.2017 00:37:42 - Starting XQuery tests
16.11.2017 00:37:42 - "Testing 1 features"
16.11.2017 00:37:42 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
16.11.2017 00:37:42 - "Statistics table: 0 ms"
16.11.2017 00:37:42 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
16.11.2017 00:37:42 - "Test Case 'Units of measure' started"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
16.11.2017 00:37:42 - "Test Case 'Units of measure' finished: PASSED"
16.11.2017 00:37:42 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
16.11.2017 00:37:43 - Releasing resources
16.11.2017 00:37:43 - Changed state from INITIALIZED to RUNNING
16.11.2017 00:37:43 - Duration: 29sec
16.11.2017 00:37:43 - TestRun finished
16.11.2017 00:37:43 - Changed state from RUNNING to COMPLETED
