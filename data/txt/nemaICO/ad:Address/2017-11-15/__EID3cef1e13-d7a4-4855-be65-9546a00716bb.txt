15.11.2017 22:31:47 - Preparing Test Run ad:Address (initiated Wed Nov 15 22:31:47 CET 2017)
15.11.2017 22:31:47 - Resolving Executable Test Suite dependencies
15.11.2017 22:31:47 - Preparing 10 Test Task:
15.11.2017 22:31:47 -  TestTask 1 (ad0b8134-c454-4727-9815-c1d715ba4ccb)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 2 (ec62de54-01e5-4972-bbd4-ad81e396f845)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.3 )'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 3 (87564b57-3dff-4ef8-af50-a1867947e4e1)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.e6800faf-2e56-47df-831a-75a96b35f33d'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 4 (b95de93f-d9e3-42c6-88e9-a43859796992)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Addresses (EID: 8aaef94b-6a4d-47ab-a5d0-70ad5cb28b08, V: 0.2.1 )'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 5 (1985fc5a-b405-48ed-9020-0f8cb4c28b94)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 6 (19f62268-7972-4374-8e8a-c8ffbce6c0f8)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Addresses (EID: 9c31fa6e-1fab-4345-bf29-6d2c129de312, V: 0.2.0 )'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 7 (5d6e8083-d124-4de0-8984-7b3d87db398a)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 8 (a56bf6fe-ad92-4efc-a153-834476660fd9)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Addresses (EID: 334bbd38-378d-4a44-8a19-5d00df919ec0, V: 0.2.1 )'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 9 (155d25fd-6799-4bca-ab03-56172baeb284)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 -  TestTask 10 (6fe3537b-db7f-4836-8ca5-e708566956f6)
15.11.2017 22:31:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Addresses (EID: 6985a681-fd81-4448-83e8-061758b9ca8c, V: 0.2.0 )'
15.11.2017 22:31:47 -  with parameters: 
15.11.2017 22:31:47 - etf.testcases = *
15.11.2017 22:31:47 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
15.11.2017 22:31:47 - Setting state to CREATED
15.11.2017 22:31:47 - Changed state from CREATED to INITIALIZING
15.11.2017 22:31:48 - Starting TestRun.3cef1e13-d7a4-4855-be65-9546a00716bb at 2017-11-15T22:31:49+01:00
15.11.2017 22:31:49 - Changed state from INITIALIZING to INITIALIZED
15.11.2017 22:31:49 - TestRunTask initialized
15.11.2017 22:31:49 - Creating new tests databases to speed up tests.
15.11.2017 22:31:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:49 - Optimizing last database etf-tdb-da969cf6-5a10-4cb6-ae4a-92c3182a66e9-0 
15.11.2017 22:31:49 - Import completed
15.11.2017 22:31:51 - Validation ended with 0 error(s)
15.11.2017 22:31:51 - Compiling test script
15.11.2017 22:31:51 - Starting XQuery tests
15.11.2017 22:31:51 - "Testing 0 features"
15.11.2017 22:31:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
15.11.2017 22:31:51 - "Statistics table: 0 ms"
15.11.2017 22:31:51 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
15.11.2017 22:31:51 - "Test Case 'Basic tests' started"
15.11.2017 22:31:51 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
15.11.2017 22:31:51 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
15.11.2017 22:31:51 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
15.11.2017 22:31:51 - "Test Case 'Basic tests' finished: PASSED"
15.11.2017 22:31:51 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
15.11.2017 22:31:51 - Releasing resources
15.11.2017 22:31:51 - TestRunTask initialized
15.11.2017 22:31:51 - Recreating new tests databases as the Test Object has changed!
15.11.2017 22:31:51 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:51 - Optimizing last database etf-tdb-da969cf6-5a10-4cb6-ae4a-92c3182a66e9-0 
15.11.2017 22:31:51 - Import completed
15.11.2017 22:31:52 - Validation ended with 0 error(s)
15.11.2017 22:31:52 - Compiling test script
15.11.2017 22:31:52 - Starting XQuery tests
15.11.2017 22:31:52 - "Testing 0 features"
15.11.2017 22:31:52 - "Indexing features (parsing errors: 0): 0 ms"
15.11.2017 22:31:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
15.11.2017 22:31:52 - "Statistics table: 0 ms"
15.11.2017 22:31:52 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
15.11.2017 22:31:52 - "Test Case 'Schema' started"
15.11.2017 22:31:52 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
15.11.2017 22:31:52 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
15.11.2017 22:31:52 - "Test Case 'Schema' finished: PASSED_MANUAL"
15.11.2017 22:31:52 - "Test Case 'Schema validation' started"
15.11.2017 22:31:52 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
15.11.2017 22:31:52 - "Validating get.xml"
15.11.2017 22:31:54 - "Duration: 2652 ms. Errors: 0."
15.11.2017 22:31:54 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 2652 ms"
15.11.2017 22:31:54 - "Test Case 'Schema validation' finished: PASSED"
15.11.2017 22:31:54 - "Test Case 'GML model' started"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Case 'GML model' finished: PASSED"
15.11.2017 22:31:54 - "Test Case 'Simple features' started"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Case 'Simple features' finished: PASSED"
15.11.2017 22:31:54 - "Test Case 'Code list values in basic data types' started"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 54 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 3 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 7 ms"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
15.11.2017 22:31:54 - "Test Case 'Code list values in basic data types' finished: PASSED"
15.11.2017 22:31:54 - "Test Case 'Constraints' started"
15.11.2017 22:31:54 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
15.11.2017 22:31:54 - "Test Case 'Constraints' finished: PASSED"
15.11.2017 22:31:54 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
15.11.2017 22:31:55 - Releasing resources
15.11.2017 22:31:55 - TestRunTask initialized
15.11.2017 22:31:55 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:55 - Validation ended with 0 error(s)
15.11.2017 22:31:55 - Compiling test script
15.11.2017 22:31:55 - Starting XQuery tests
15.11.2017 22:31:55 - "Testing 0 features"
15.11.2017 22:31:55 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ad/ad-gml/ets-ad-gml-bsxets.xml"
15.11.2017 22:31:55 - "Statistics table: 0 ms"
15.11.2017 22:31:55 - "Test Suite 'Conformance class: GML application schemas, Addresses' started"
15.11.2017 22:31:55 - "Test Case 'Basic test' started"
15.11.2017 22:31:55 - "Test Assertion 'ad-gml.a.1: Address feature in dataset': FAILED - 0 ms"
15.11.2017 22:31:55 - "Test Case 'Basic test' finished: FAILED"
15.11.2017 22:31:55 - "Test Suite 'Conformance class: GML application schemas, Addresses' finished: FAILED"
15.11.2017 22:31:56 - Releasing resources
15.11.2017 22:31:56 - TestRunTask initialized
15.11.2017 22:31:56 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:56 - Validation ended with 0 error(s)
15.11.2017 22:31:56 - Compiling test script
15.11.2017 22:31:56 - Starting XQuery tests
15.11.2017 22:31:56 - "Testing 0 features"
15.11.2017 22:31:56 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ad/ad-as/ets-ad-as-bsxets.xml"
15.11.2017 22:31:56 - "Statistics table: 0 ms"
15.11.2017 22:31:56 - "Test Suite 'Conformance class: Application schema, Addresses' started"
15.11.2017 22:31:56 - "Test Case 'Code list values' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.1: GeometryMethodValue attributes': PASSED - 5 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.2: GeometrySpecificationValue attributes': PASSED - 5 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.3: LocatorDesignatorTypeValue attributes': PASSED - 5 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.4: LocatorLevelValue attributes': PASSED - 3 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.5: LocatorNameTypeValue attributes': PASSED - 4 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.6: PartTypeValue attributes': PASSED - 4 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.a.7: StatusValue attributes': PASSED - 3 ms"
15.11.2017 22:31:56 - "Test Case 'Code list values' finished: PASSED"
15.11.2017 22:31:56 - "Test Case 'Constraints' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.b.1: An address shall have an admin unit address component spatial object whose level is 1 (Country).': PASSED - 0 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.b.2: An address shall have exactly one default geographic position (default attribute of GeographicPosition must be true).': PASSED - 0 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.b.3: Either a post code or a post name is required.': PASSED - 0 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.b.4: Either a designator or a name is required.': PASSED - 0 ms"
15.11.2017 22:31:56 - "Test Case 'Constraints' finished: PASSED"
15.11.2017 22:31:56 - "Test Case 'Address Multiple Position' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.c.1: Verify whether each address with multiple positions contains different values for each specification attribute.': PASSED - 0 ms"
15.11.2017 22:31:56 - "Test Case 'Address Multiple Position' finished: PASSED"
15.11.2017 22:31:56 - "Test Case 'Address Position' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.d.1: Manual review - Verify whether, in the data set, the position of the address is represented by the coordinates of the actual location with the best available accuracy.': PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Address Position' finished: PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Country and Address Components' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.e.1: Verify that an address has associations to the address components necessary to the unambiguous identification and location of the address instance.': PASSED - 0 ms"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.e.2: Manual review': PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Country and Address Components' finished: PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Parent address' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.f.1: Manual review - Verify that the association role “parentAddress” is populated for all addresses which are connected to a parent (or main) address.': PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Parent address' finished: PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Unambiguousness' started"
15.11.2017 22:31:56 - "Test Assertion 'ad-as.g.1: Manual review - Verify whether the withinScopeOf association role is populated for all locators which are assigned according to rules that seek to ensure unambiguousness within a specific address component.': PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Case 'Unambiguousness' finished: PASSED_MANUAL"
15.11.2017 22:31:56 - "Test Suite 'Conformance class: Application schema, Addresses' finished: PASSED_MANUAL"
15.11.2017 22:31:57 - Releasing resources
15.11.2017 22:31:57 - TestRunTask initialized
15.11.2017 22:31:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:57 - Validation ended with 0 error(s)
15.11.2017 22:31:57 - Compiling test script
15.11.2017 22:31:57 - Starting XQuery tests
15.11.2017 22:31:57 - "Testing 0 features"
15.11.2017 22:31:57 - "Indexing features (parsing errors: 0): 0 ms"
15.11.2017 22:31:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
15.11.2017 22:31:57 - "Statistics table: 0 ms"
15.11.2017 22:31:57 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
15.11.2017 22:31:57 - "Test Case 'Version consistency' started"
15.11.2017 22:31:57 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
15.11.2017 22:31:57 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
15.11.2017 22:31:57 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
15.11.2017 22:31:57 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
15.11.2017 22:31:57 - "Test Case 'Temporal consistency' started"
15.11.2017 22:31:57 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
15.11.2017 22:31:57 - "Test Case 'Temporal consistency' finished: PASSED"
15.11.2017 22:31:57 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
15.11.2017 22:31:57 - Releasing resources
15.11.2017 22:31:57 - TestRunTask initialized
15.11.2017 22:31:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:57 - Validation ended with 0 error(s)
15.11.2017 22:31:57 - Compiling test script
15.11.2017 22:31:57 - Starting XQuery tests
15.11.2017 22:31:57 - "Testing 0 features"
15.11.2017 22:31:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ad/ad-dc/ets-ad-dc-bsxets.xml"
15.11.2017 22:31:57 - "Statistics table: 0 ms"
15.11.2017 22:31:57 - "Test Suite 'Conformance class: Data consistency, Addresses' started"
15.11.2017 22:31:57 - "Test Case 'Additional theme-specific consistency rules' started"
15.11.2017 22:31:57 - "Test Assertion 'ad-dc.a.1: Test always passes': PASSED - 0 ms"
15.11.2017 22:31:57 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
15.11.2017 22:31:57 - "Test Suite 'Conformance class: Data consistency, Addresses' finished: PASSED"
15.11.2017 22:31:58 - Releasing resources
15.11.2017 22:31:58 - TestRunTask initialized
15.11.2017 22:31:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:58 - Validation ended with 0 error(s)
15.11.2017 22:31:58 - Compiling test script
15.11.2017 22:31:58 - Starting XQuery tests
15.11.2017 22:31:58 - "Testing 0 features"
15.11.2017 22:31:58 - "Indexing features (parsing errors: 0): 0 ms"
15.11.2017 22:31:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
15.11.2017 22:31:58 - "Statistics table: 0 ms"
15.11.2017 22:31:58 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
15.11.2017 22:31:58 - "Test Case 'Coordinate reference systems (CRS)' started"
15.11.2017 22:31:58 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Case 'Coordinate reference systems (CRS)' finished: PASSED"
15.11.2017 22:31:58 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: PASSED"
15.11.2017 22:31:58 - Releasing resources
15.11.2017 22:31:58 - TestRunTask initialized
15.11.2017 22:31:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:58 - Validation ended with 0 error(s)
15.11.2017 22:31:58 - Compiling test script
15.11.2017 22:31:58 - Starting XQuery tests
15.11.2017 22:31:58 - "Testing 0 features"
15.11.2017 22:31:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ad/ad-ia/ets-ad-ia-bsxets.xml"
15.11.2017 22:31:58 - "Statistics table: 0 ms"
15.11.2017 22:31:58 - "Test Suite 'Conformance class: Information accessibility, Addresses' started"
15.11.2017 22:31:58 - "Test Case 'Code lists' started"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Case 'Code lists' finished: PASSED"
15.11.2017 22:31:58 - "Test Case 'Feature references' started"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.1: Address.component': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.2: Address.parcel': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.3: Address.parentAddress': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.4: Address.building': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.5: AddressAreaName.namedPlace': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.6: AddressComponent.situatedWithin': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.7: AdminUnitName.adminUnit': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.8: ThoroughfareName.transportLink': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.9: Address.locator.AddressLocator.withinScopeOf': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Assertion 'ad-ia.b.10: AddressRepresentation.addressFeature': PASSED - 0 ms"
15.11.2017 22:31:58 - "Test Case 'Feature references' finished: PASSED"
15.11.2017 22:31:58 - "Test Suite 'Conformance class: Information accessibility, Addresses' finished: PASSED"
15.11.2017 22:31:59 - Releasing resources
15.11.2017 22:31:59 - TestRunTask initialized
15.11.2017 22:31:59 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:59 - Validation ended with 0 error(s)
15.11.2017 22:31:59 - Compiling test script
15.11.2017 22:31:59 - Starting XQuery tests
15.11.2017 22:31:59 - "Testing 0 features"
15.11.2017 22:31:59 - "Indexing features (parsing errors: 0): 0 ms"
15.11.2017 22:31:59 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
15.11.2017 22:31:59 - "Statistics table: 0 ms"
15.11.2017 22:31:59 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
15.11.2017 22:31:59 - "Test Case 'Spatial reference systems' started"
15.11.2017 22:31:59 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': PASSED - 0 ms"
15.11.2017 22:31:59 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
15.11.2017 22:31:59 - "Test Case 'Spatial reference systems' finished: PASSED"
15.11.2017 22:31:59 - "Test Case 'Temporal reference systems' started"
15.11.2017 22:31:59 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
15.11.2017 22:31:59 - "Test Case 'Temporal reference systems' finished: PASSED"
15.11.2017 22:31:59 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: PASSED"
15.11.2017 22:31:59 - Releasing resources
15.11.2017 22:31:59 - TestRunTask initialized
15.11.2017 22:31:59 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:31:59 - Validation ended with 0 error(s)
15.11.2017 22:31:59 - Compiling test script
15.11.2017 22:31:59 - Starting XQuery tests
15.11.2017 22:31:59 - "Testing 0 features"
15.11.2017 22:31:59 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ad/ad-rs/ets-ad-rs-bsxets.xml"
15.11.2017 22:31:59 - "Statistics table: 0 ms"
15.11.2017 22:31:59 - "Test Suite 'Conformance class: Reference systems, Addresses' started"
15.11.2017 22:31:59 - "Test Case 'Additional theme-specific rules for reference systems' started"
15.11.2017 22:31:59 - "Test Assertion 'ad-rs.a.1: Test always passes': PASSED - 0 ms"
15.11.2017 22:31:59 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
15.11.2017 22:31:59 - "Test Suite 'Conformance class: Reference systems, Addresses' finished: PASSED"
15.11.2017 22:32:00 - Releasing resources
15.11.2017 22:32:00 - Changed state from INITIALIZED to RUNNING
15.11.2017 22:32:00 - Duration: 12sec
15.11.2017 22:32:00 - TestRun finished
15.11.2017 22:32:00 - Changed state from RUNNING to COMPLETED
