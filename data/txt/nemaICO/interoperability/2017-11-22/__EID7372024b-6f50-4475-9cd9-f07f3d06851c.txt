22.11.2017 22:16:42 - Preparing Test Run interoperability (initiated Wed Nov 22 22:16:42 CET 2017)
22.11.2017 22:16:42 - Resolving Executable Test Suite dependencies
22.11.2017 22:16:42 - Preparing 10 Test Task:
22.11.2017 22:16:42 -  TestTask 1 (4eff370f-563a-4cb7-954b-9099855c2f16)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 2 (1950401c-a23d-40dc-830a-dce24a822bef)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'LAZY.09820daf-62b2-4fa3-a95f-56a0d2b7c4d8'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 3 (4606f8bc-841c-4813-b415-7a020120e2d4)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'LAZY.8222c253-8468-4b94-a46b-2d1af1698a65'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 4 (d1d529d8-ba05-4471-b6ad-abeb01886af4)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'Conformance class: Application schema, Protected Sites Simple (EID: 4c53a8c7-7cac-4531-982b-d03eb48ffa77, V: 0.2.1 )'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 5 (624632f6-dc79-4d6e-bad5-e2e298ad904f)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 6 (83cea243-b1c5-4c0f-a49b-d7a2476e09f5)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'Conformance class: Data consistency, Protected Sites (EID: 7831c8b4-f666-4534-838a-137b30bfecbe, V: 0.2.0 )'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 7 (b0b271ac-6c03-4ea0-8ac1-80853f13ef31)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 8 (380127ed-c383-49c9-a2f1-4e17622e399c)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Protected Sites (EID: b529e8fa-b9f8-4758-acea-1d2af744599f, V: 0.2.2 )'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 9 (35a9d02c-3efc-43bf-a6db-825114381f91)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 -  TestTask 10 (940236fe-2719-4617-b20f-ecbcda46c596)
22.11.2017 22:16:42 -  will perform tests on Test Object 'ows.xml' by using Executable Test Suite 'Conformance class: Reference systems, Protected Sites (EID: 828410c1-53f2-4683-bded-481ad9d4d3e9, V: 0.2.0 )'
22.11.2017 22:16:42 -  with parameters: 
22.11.2017 22:16:42 - etf.testcases = *
22.11.2017 22:16:42 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
22.11.2017 22:16:42 - Setting state to CREATED
22.11.2017 22:16:42 - Changed state from CREATED to INITIALIZING
22.11.2017 22:16:42 - Starting TestRun.7372024b-6f50-4475-9cd9-f07f3d06851c at 2017-11-22T22:16:44+01:00
22.11.2017 22:16:44 - Changed state from INITIALIZING to INITIALIZED
22.11.2017 22:16:44 - TestRunTask initialized
22.11.2017 22:16:44 - Creating new tests databases to speed up tests.
22.11.2017 22:16:44 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:16:44 - Optimizing last database etf-tdb-3ed6f7f1-3ab0-4fdc-a869-bc20940200bd-0 
22.11.2017 22:16:44 - Import completed
22.11.2017 22:16:45 - Validation ended with 0 error(s)
22.11.2017 22:16:45 - Compiling test script
22.11.2017 22:16:45 - Starting XQuery tests
22.11.2017 22:16:45 - "Testing 1 features"
22.11.2017 22:16:45 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
22.11.2017 22:16:45 - "Statistics table: 0 ms"
22.11.2017 22:16:45 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
22.11.2017 22:16:45 - "Test Case 'Basic tests' started"
22.11.2017 22:16:45 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
22.11.2017 22:16:45 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
22.11.2017 22:16:45 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
22.11.2017 22:16:45 - "Test Case 'Basic tests' finished: PASSED"
22.11.2017 22:16:45 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
22.11.2017 22:16:48 - Releasing resources
22.11.2017 22:16:48 - TestRunTask initialized
22.11.2017 22:16:48 - Recreating new tests databases as the Test Object has changed!
22.11.2017 22:16:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:16:48 - Optimizing last database etf-tdb-3ed6f7f1-3ab0-4fdc-a869-bc20940200bd-0 
22.11.2017 22:16:48 - Import completed
22.11.2017 22:16:48 - Validation ended with 0 error(s)
22.11.2017 22:16:48 - Compiling test script
22.11.2017 22:16:48 - Starting XQuery tests
22.11.2017 22:16:48 - "Testing 1 features"
22.11.2017 22:16:48 - "Indexing features (parsing errors: 0): 141 ms"
22.11.2017 22:16:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
22.11.2017 22:16:48 - "Statistics table: 0 ms"
22.11.2017 22:16:48 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
22.11.2017 22:16:48 - "Test Case 'Schema' started"
22.11.2017 22:16:48 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
22.11.2017 22:16:48 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
22.11.2017 22:16:48 - "Test Case 'Schema' finished: PASSED_MANUAL"
22.11.2017 22:16:48 - "Test Case 'Schema validation' started"
22.11.2017 22:16:48 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
22.11.2017 22:16:48 - "Validating ows.xml"
22.11.2017 22:16:59 - "Duration: 10703 ms. Errors: 40."
22.11.2017 22:16:59 - "Test Assertion 'gmlas.b.2: validate XML documents': FAILED - 10704 ms"
22.11.2017 22:16:59 - "Test Case 'Schema validation' finished: FAILED"
22.11.2017 22:16:59 - "Test Case 'GML model' started"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': FAILED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Case 'GML model' finished: FAILED"
22.11.2017 22:16:59 - "Test Case 'Simple features' started"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 1 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 1 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': FAILED - 177 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
22.11.2017 22:16:59 - "Test Case 'Simple features' finished: FAILED"
22.11.2017 22:16:59 - "Test Case 'Code list values in basic data types' started"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 31 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 21 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 22 ms"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 17 ms"
22.11.2017 22:16:59 - "Test Case 'Code list values in basic data types' finished: PASSED"
22.11.2017 22:16:59 - "Test Case 'Constraints' started"
22.11.2017 22:16:59 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': FAILED - 0 ms"
22.11.2017 22:16:59 - "Test Case 'Constraints' finished: FAILED"
22.11.2017 22:16:59 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: FAILED"
22.11.2017 22:17:01 - Releasing resources
22.11.2017 22:17:01 - TestRunTask initialized
22.11.2017 22:17:01 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:01 - Validation ended with 0 error(s)
22.11.2017 22:17:01 - Compiling test script
22.11.2017 22:17:01 - Starting XQuery tests
22.11.2017 22:17:01 - "Testing 1 features"
22.11.2017 22:17:01 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ps/ps-gml/ets-ps-gml-bsxets.xml"
22.11.2017 22:17:01 - "Statistics table: 0 ms"
22.11.2017 22:17:01 - "Test Suite 'Conformance class: GML application schemas, Protected Sites' started"
22.11.2017 22:17:01 - "Test Case 'Basic test' started"
22.11.2017 22:17:01 - "Test Assertion 'ps-gml.a.1: Protected site feature in dataset': PASSED - 0 ms"
22.11.2017 22:17:01 - "Test Case 'Basic test' finished: PASSED"
22.11.2017 22:17:01 - "Test Suite 'Conformance class: GML application schemas, Protected Sites' finished: PASSED"
22.11.2017 22:17:01 - Releasing resources
22.11.2017 22:17:01 - TestRunTask initialized
22.11.2017 22:17:01 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:01 - Validation ended with 0 error(s)
22.11.2017 22:17:01 - Compiling test script
22.11.2017 22:17:01 - Starting XQuery tests
22.11.2017 22:17:01 - "Testing 1 features"
22.11.2017 22:17:01 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ps/ps-as/ets-ps-as-bsxets.xml"
22.11.2017 22:17:01 - "Statistics table: 0 ms"
22.11.2017 22:17:01 - "Test Suite 'Conformance class: Application schema, Protected Sites Simple' started"
22.11.2017 22:17:01 - "Test Case 'Code list values' started"
22.11.2017 22:17:01 - "Test Assertion 'ps-as.a.1: extensions to code lists in property 'ps:designation'': PASSED - 66 ms"
22.11.2017 22:17:01 - "Test Assertion 'ps-as.a.2: extensions to code lists in property 'ps:designationScheme'': PASSED - 0 ms"
22.11.2017 22:17:01 - "Test Case 'Code list values' finished: PASSED"
22.11.2017 22:17:01 - "Test Case 'Constraints' started"
22.11.2017 22:17:01 - "Test Assertion 'ps-as.b.1: Sites must use designations from an appropriate designation scheme': PASSED - 44 ms"
22.11.2017 22:17:01 - "Test Case 'Constraints' finished: PASSED"
22.11.2017 22:17:01 - "Test Suite 'Conformance class: Application schema, Protected Sites Simple' finished: PASSED"
22.11.2017 22:17:02 - Releasing resources
22.11.2017 22:17:02 - TestRunTask initialized
22.11.2017 22:17:02 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:02 - Validation ended with 0 error(s)
22.11.2017 22:17:02 - Compiling test script
22.11.2017 22:17:02 - Starting XQuery tests
22.11.2017 22:17:02 - "Testing 1 features"
22.11.2017 22:17:02 - "Indexing features (parsing errors: 0): 135 ms"
22.11.2017 22:17:02 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
22.11.2017 22:17:02 - "Statistics table: 0 ms"
22.11.2017 22:17:02 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
22.11.2017 22:17:02 - "Test Case 'Version consistency' started"
22.11.2017 22:17:02 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 1 ms"
22.11.2017 22:17:02 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
22.11.2017 22:17:02 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
22.11.2017 22:17:02 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
22.11.2017 22:17:02 - "Test Case 'Temporal consistency' started"
22.11.2017 22:17:02 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
22.11.2017 22:17:02 - "Test Case 'Temporal consistency' finished: PASSED"
22.11.2017 22:17:02 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
22.11.2017 22:17:03 - Releasing resources
22.11.2017 22:17:03 - TestRunTask initialized
22.11.2017 22:17:03 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:03 - Validation ended with 0 error(s)
22.11.2017 22:17:03 - Compiling test script
22.11.2017 22:17:03 - Starting XQuery tests
22.11.2017 22:17:03 - "Testing 1 features"
22.11.2017 22:17:03 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ps/ps-dc/ets-ps-dc-bsxets.xml"
22.11.2017 22:17:03 - "Statistics table: 0 ms"
22.11.2017 22:17:03 - "Test Suite 'Conformance class: Data consistency, Protected Sites' started"
22.11.2017 22:17:03 - "Test Case 'Additional theme-specific consistency rules' started"
22.11.2017 22:17:03 - "Test Assertion 'ps-dc.a.1: Test always passes': PASSED - 0 ms"
22.11.2017 22:17:03 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
22.11.2017 22:17:03 - "Test Suite 'Conformance class: Data consistency, Protected Sites' finished: PASSED"
22.11.2017 22:17:03 - Releasing resources
22.11.2017 22:17:03 - TestRunTask initialized
22.11.2017 22:17:03 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:03 - Validation ended with 0 error(s)
22.11.2017 22:17:03 - Compiling test script
22.11.2017 22:17:03 - Starting XQuery tests
22.11.2017 22:17:03 - "Testing 1 features"
22.11.2017 22:17:03 - "Indexing features (parsing errors: 0): 139 ms"
22.11.2017 22:17:03 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
22.11.2017 22:17:03 - "Statistics table: 0 ms"
22.11.2017 22:17:03 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
22.11.2017 22:17:03 - "Test Case 'Coordinate reference systems (CRS)' started"
22.11.2017 22:17:03 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
22.11.2017 22:17:03 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
22.11.2017 22:17:03 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
22.11.2017 22:17:04 - Releasing resources
22.11.2017 22:17:04 - TestRunTask initialized
22.11.2017 22:17:04 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:04 - Validation ended with 0 error(s)
22.11.2017 22:17:04 - Compiling test script
22.11.2017 22:17:04 - Starting XQuery tests
22.11.2017 22:17:04 - "Testing 1 features"
22.11.2017 22:17:04 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ps/ps-ia/ets-ps-ia-bsxets.xml"
22.11.2017 22:17:04 - "Statistics table: 0 ms"
22.11.2017 22:17:04 - "Test Suite 'Conformance class: Information accessibility, Protected Sites' started"
22.11.2017 22:17:04 - "Test Case 'Code lists' started"
22.11.2017 22:17:04 - "Test Assertion 'ps-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
22.11.2017 22:17:04 - "Test Case 'Code lists' finished: PASSED"
22.11.2017 22:17:04 - "Test Suite 'Conformance class: Information accessibility, Protected Sites' finished: PASSED"
22.11.2017 22:17:05 - Releasing resources
22.11.2017 22:17:05 - TestRunTask initialized
22.11.2017 22:17:05 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:05 - Validation ended with 0 error(s)
22.11.2017 22:17:05 - Compiling test script
22.11.2017 22:17:05 - Starting XQuery tests
22.11.2017 22:17:05 - "Testing 1 features"
22.11.2017 22:17:05 - "Indexing features (parsing errors: 0): 167 ms"
22.11.2017 22:17:05 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
22.11.2017 22:17:05 - "Statistics table: 0 ms"
22.11.2017 22:17:05 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
22.11.2017 22:17:05 - "Test Case 'Spatial reference systems' started"
22.11.2017 22:17:05 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 1 ms"
22.11.2017 22:17:05 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
22.11.2017 22:17:05 - "Test Case 'Spatial reference systems' finished: FAILED"
22.11.2017 22:17:05 - "Test Case 'Temporal reference systems' started"
22.11.2017 22:17:05 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 1 ms"
22.11.2017 22:17:05 - "Test Case 'Temporal reference systems' finished: PASSED"
22.11.2017 22:17:05 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
22.11.2017 22:17:06 - Releasing resources
22.11.2017 22:17:06 - TestRunTask initialized
22.11.2017 22:17:06 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 22:17:06 - Validation ended with 0 error(s)
22.11.2017 22:17:06 - Compiling test script
22.11.2017 22:17:06 - Starting XQuery tests
22.11.2017 22:17:06 - "Testing 1 features"
22.11.2017 22:17:06 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-ps/ps-rs/ets-ps-rs-bsxets.xml"
22.11.2017 22:17:06 - "Statistics table: 1 ms"
22.11.2017 22:17:06 - "Test Suite 'Conformance class: Reference systems, Protected Sites' started"
22.11.2017 22:17:06 - "Test Case 'Additional theme-specific rules for reference systems' started"
22.11.2017 22:17:06 - "Test Assertion 'ps-rs.a.1: Test always passes': PASSED - 0 ms"
22.11.2017 22:17:06 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
22.11.2017 22:17:06 - "Test Suite 'Conformance class: Reference systems, Protected Sites' finished: PASSED"
22.11.2017 22:17:06 - Releasing resources
22.11.2017 22:17:06 - Changed state from INITIALIZED to RUNNING
22.11.2017 22:17:06 - Duration: 24sec
22.11.2017 22:17:06 - TestRun finished
22.11.2017 22:17:06 - Changed state from RUNNING to COMPLETED
